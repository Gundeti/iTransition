<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Stores historical counts of projections, derived from a reporting snapshot at the end of each month.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <fields>
        <fullName>Category__c</fullName>
        <description>Category of the SKU Projection</description>
        <externalId>false</externalId>
        <label>Category</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Count__c</fullName>
        <description>Projected Count of SKUs</description>
        <externalId>false</externalId>
        <label>Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Date_of_Projection__c</fullName>
        <description>Date (Month) for which these counts are applicable.</description>
        <externalId>false</externalId>
        <label>Date of Projection</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Projection_Type__c</fullName>
        <description>Can be &apos;New&apos; or &apos;Disco&apos;</description>
        <externalId>false</externalId>
        <label>Projection Type</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Source__c</fullName>
        <description>Indicates the projection source. Could be G-11 Unchanged, G-11 Modified or Manual</description>
        <externalId>false</externalId>
        <label>Source</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Historical Projection Count</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Category__c</columns>
        <columns>NAME</columns>
        <columns>Projection_Type__c</columns>
        <columns>Date_of_Projection__c</columns>
        <columns>Count__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>HPROJ-{000000}</displayFormat>
        <label>Historical Projection Counts Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Historical_Projection_Counts</pluralLabel>
    <searchLayouts/>
    <sharingModel>Read</sharingModel>
</CustomObject>
