<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>false</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>false</enableSharing>
    <enableStreamingApi>false</enableStreamingApi>
    <fields>
        <fullName>Brand_Code_Source__c</fullName>
        <externalId>false</externalId>
        <label>Brand_Code_Source</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Brand_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Brand__r.Brand_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Brand Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Brand_Description__c</fullName>
        <externalId>false</externalId>
        <label>Brand Description</label>
        <length>100000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Brand__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Brand</label>
        <referenceTo>Brand__c</referenceTo>
        <relationshipLabel>Brand Actuals Staging</relationshipLabel>
        <relationshipName>Brand_Actuals_Staging</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Category__c</fullName>
        <externalId>false</externalId>
        <label>Category</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Date__c</fullName>
        <externalId>false</externalId>
        <label>Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>EOH_Inventory__c</fullName>
        <externalId>false</externalId>
        <label>EOH Inventory</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>GBU__c</fullName>
        <description>the BU</description>
        <externalId>false</externalId>
        <label>GBU</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetName>GBU</valueSetName>
        </valueSet>
    </fields>
    <fields>
        <fullName>Group__c</fullName>
        <externalId>false</externalId>
        <label>Group</label>
        <length>255</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Net_Invoice_Value_MM__c</fullName>
        <externalId>false</externalId>
        <label>Net Invoice Value MM</label>
        <precision>16</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SKU_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Brand__r.SKU__r.SKU__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>SKU Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SKU_Description__c</fullName>
        <externalId>false</externalId>
        <label>SKU Description</label>
        <length>100000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Shipment_in_MSU__c</fullName>
        <externalId>false</externalId>
        <label>Shipment in MSU</label>
        <precision>16</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Sku_Code_Source__c</fullName>
        <externalId>false</externalId>
        <label>Sku_Code_Source</label>
        <length>50</length>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Active &amp; Planned</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Remnant &amp; Historical</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Three_Month_MM_Net_Invoice__c</fullName>
        <externalId>false</externalId>
        <label>Three Month MM Net Invoice</label>
        <precision>16</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Three_Month_MSU_Shipment__c</fullName>
        <externalId>false</externalId>
        <label>Three Month MSU Shipment</label>
        <precision>16</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Quantity__c</fullName>
        <externalId>false</externalId>
        <label>Total Quantity</label>
        <precision>16</precision>
        <required>false</required>
        <scale>6</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Brand Actual Staging</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Date__c</columns>
        <columns>Brand__c</columns>
        <columns>Brand_Code__c</columns>
        <columns>SKU_Code__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Brand Actual Staging Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Brand Actuals Staging</pluralLabel>
    <searchLayouts/>
    <sharingModel>Private</sharingModel>
    <validationRules>
        <fullName>Invalid_Character_in_input_File</fullName>
        <active>true</active>
        <description>Catch invalid characters in input files (E+)</description>
        <errorConditionFormula>OR( CONTAINS( Brand_Code_Source__c , &apos;E+&apos;) ,CONTAINS(  Sku_Code_Source__c , &apos;E+&apos;))</errorConditionFormula>
        <errorMessage>SKU or BrandCode contains an invalid character</errorMessage>
    </validationRules>
</CustomObject>
