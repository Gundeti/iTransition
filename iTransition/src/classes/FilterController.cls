public class FilterController {
    
    public class customWrapper
    {
        @AuraEnabled
        public list<User> lstUser;
        @AuraEnabled
        public date startDate;
        @AuraEnabled
        public date endDate;
                
        public customWrapper(list<User> lstUser, date startDate, date endDate){
            this.lstUser = lstUser;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    } 
    
    @AuraEnabled
    public static customWrapper userSettings(){
        
        Id userId = UserInfo.getUserId();
        
        list<User> lstUser = [SELECT Id, Name, Category__c, GBU__c from User WHERE id=:userId ];
        
        date endDate = LatestDates__c.getOrgDefaults().Date__c;
        date startDate = endDate.addMonths(-11);
        customWrapper customData = new customWrapper(lstUser,startDate,endDate);
        system.debug('customData------'+customData);
        return customData;
    }
}