@isTest
public class TestRunProcessBatch {

    static testMethod void TestBatchProcessProjections() {

         List<BatchStatus__c> bsList = new List<BatchStatus__c>();
        List<BatchJobStatus__c> bjsList = new List<BatchJobStatus__c>();
        bsList.add(new BatchStatus__c(JobOrder__c=1,JobType__c='Inventory',JobName__c='BatchLoadInventory'));
        bsList.add(new BatchStatus__c(JobOrder__c=2,JobType__c='Inventory',JobName__c='BatchSetInventoryUpdateFlag'));
        bsList.add(new BatchStatus__c(JobOrder__c=3,JobType__c='Inventory',JobName__c='BatchUnSetInventoryUpdateFlag'));
        bsList.add(new BatchStatus__c(JobOrder__c=4,JobType__c='Inventory',JobName__c='BatchUpdateInventoryMissing'));
        bsList.add(new BatchStatus__c(JobOrder__c=5,JobType__c='Inventory',JobName__c='BatchIdentifyPlantInvForElim'));
        bsList.add(new BatchStatus__c(JobOrder__c=6,JobType__c='Inventory',JobName__c='BatchUpdateInvStatusToRdyToSteam'));
        bsList.add(new BatchStatus__c(JobOrder__c=7,JobType__c='Inventory',JobName__c='CreateInventoryTasks'));

        bsList.add(new BatchStatus__c(JobOrder__c=1,JobType__c='Actuals',JobName__c='BatchBrandActualStaging'));
        bsList.add(new BatchStatus__c(JobOrder__c=2,JobType__c='Actuals',JobName__c='MarkMissingBrandCodesAsDeleted'));
        bsList.add(new BatchStatus__c(JobOrder__c=3,JobType__c='Actuals',JobName__c='BatchMarkMissingBrandCodesFound'));
        bsList.add(new BatchStatus__c(JobOrder__c=4,JobType__c='Actuals',JobName__c='BatchUpdateSKULatestStatusUpdateDates'));

        bsList.add(new BatchStatus__c(JobOrder__c=1,JobType__c='Projections',JobName__c='BatchBrandProjectionStaging'));

        insert bsList;

        bjsList.add(new BatchJobStatus__c(Name='Inventory',	isJobRunning__c=false));
        bjsList.add(new BatchJobStatus__c(Name='Actuals',	isJobRunning__c=false));
        bjsList.add(new BatchJobStatus__c(Name='Projections',	isJobRunning__c=false));

        insert bjsList;

        Id orgId = [Select id from Organization].Id;
        Date actualDate = System.today().addMonths(-1).toStartOfMonth();
        Date projUpdateDate = System.today().addMonths(-1).toStartOfMonth();
        Date invUpdateDate = System.today().addMonths(-2).toStartOfMonth();
     
        DataFactory.createLatestDates(orgID, actualDate, projUpdateDate,invUpdateDate);
       
          List<SKU__c> skuList = DataFactory.createSKUs(40, 3, 'Air Care', 'Active & Planned','All Air Care','Home Care');
         Date projDate = System.today().addMonths(-1).toStartOfMonth();
          List<SKU__c> skuList2 = new List<SKU__c>();
          List<Brand__c> brandList = new List<Brand__c>();
          List<Brand__c> brandList2 = new List<Brand__c>();
          List<Brand__c> brandList3 = new List<Brand__c>();
          Integer count = 0;
          for(Integer r = 0 ; r < skuList.size();r++) {
            if(count < 10) brandList.addAll(skuList[r].Brands__r);
            else if(count >=10 && count <15)  brandList2.addAll(skuList[r].Brands__r);
            else if(count >= 15)  {
                brandList3.addAll(skuList[r].Brands__r);
                skuList2.add(skuList[r]);
                skuList.remove(r);

            }
            count++;      

        }        
        //DataFactory.createSKUProjection(skuList,20,'manual',true,System.today().addMonths(-1).toStartOfMonth(),'Non-Customized Disco');
        List<Brand_Projection_Staging__c> bpsList = DataFactory.createBrandProjectionStaging(200,'Home Care','Non-Customized Disco','ADW',System.today().addMonths(-1).toStartOfMonth(),'Home Care');

         RunProcessBatchController runProcess = new RunProcessBatchController();
       // runProcess.ActualsRun = true;
        runProcess.GBU = 'Home Care';
        Test.startTest();
        runProcess.RunProjections();
        runProcess = new RunProcessBatchController();
         Test.stopTest();
       
        runProcess.PollJobs();   

        System.assert(runProcess.BrandProjectionsJobs.size() > 0);
        System.assertEquals(false,runProcess.ProjectionsRun);
        System.assertEquals(false, [Select isJobRunning__c from BatchJobStatus__c where Name='Projections'].IsJobRunning__c);
       List<BatchStatus__c> bsPostRunCheck = [Select JobName__c,JobOrder__c,JobStatus__c,JobErrors__c,JobStartDate__c,JobEndDate__c from BatchStatus__c where JobType__c='Projections' order by JobOrder__c ASC];
        for(BatchStatus__c bs:bsPostRunCheck) {
            System.assertEquals('Completed',bs.JobStatus__c);
            System.assertEquals(null,bs.JobErrors__c);
        }

        runProcess.GBU = 'SelectGBU';
        runProcess.RunProjections();
        bsPostRunCheck = [Select JobName__c,JobOrder__c,JobStatus__c,JobErrors__c,JobStartDate__c,JobEndDate__c from BatchStatus__c where JobType__c='Projections' and JobOrder__c = 1];
        for(BatchStatus__c bs:bsPostRunCheck) {   
            System.assertEquals('Error',bs.JobStatus__c);
            System.assertEquals('Please select a valid GBU',bs.JobErrors__c);
        }
        System.assertEquals(false, [Select isJobRunning__c from BatchJobStatus__c where Name='Projections'].IsJobRunning__c);     

    }
    static testmethod void TestBatchProcessActuals() {
        List<BatchStatus__c> bsList = new List<BatchStatus__c>();
        List<BatchJobStatus__c> bjsList = new List<BatchJobStatus__c>();
        bsList.add(new BatchStatus__c(JobOrder__c=1,JobType__c='Inventory',JobName__c='BatchLoadInventory'));
        bsList.add(new BatchStatus__c(JobOrder__c=2,JobType__c='Inventory',JobName__c='BatchSetInventoryUpdateFlag'));
        bsList.add(new BatchStatus__c(JobOrder__c=3,JobType__c='Inventory',JobName__c='BatchUnSetInventoryUpdateFlag'));
        bsList.add(new BatchStatus__c(JobOrder__c=4,JobType__c='Inventory',JobName__c='BatchUpdateInventoryMissing'));
        bsList.add(new BatchStatus__c(JobOrder__c=5,JobType__c='Inventory',JobName__c='BatchIdentifyPlantInvForElim'));
        bsList.add(new BatchStatus__c(JobOrder__c=6,JobType__c='Inventory',JobName__c='BatchUpdateInvStatusToRdyToSteam'));
        bsList.add(new BatchStatus__c(JobOrder__c=7,JobType__c='Inventory',JobName__c='CreateInventoryTasks'));

        bsList.add(new BatchStatus__c(JobOrder__c=1,JobType__c='Actuals',JobName__c='BatchBrandActualStaging'));
        bsList.add(new BatchStatus__c(JobOrder__c=2,JobType__c='Actuals',JobName__c='MarkMissingBrandCodesAsDeleted'));
        bsList.add(new BatchStatus__c(JobOrder__c=3,JobType__c='Actuals',JobName__c='BatchMarkMissingBrandCodesFound'));
        bsList.add(new BatchStatus__c(JobOrder__c=4,JobType__c='Actuals',JobName__c='BatchUpdateSKULatestStatusUpdateDates'));

        bsList.add(new BatchStatus__c(JobOrder__c=1,JobType__c='Projections',JobName__c='BatchBrandProjectionStaging'));

        insert bsList;

        bjsList.add(new BatchJobStatus__c(Name='Inventory',	isJobRunning__c=false));
        bjsList.add(new BatchJobStatus__c(Name='Actuals',	isJobRunning__c=false));
        bjsList.add(new BatchJobStatus__c(Name='Projections',	isJobRunning__c=false));

        insert bjsList;

        Id orgId = [Select id from Organization].Id;
        Date actualDate = System.today().addMonths(-1).toStartOfMonth();
        Date projUpdateDate = System.today().addMonths(-1).toStartOfMonth();
        Date invUpdateDate = System.today().addMonths(-2).toStartOfMonth();
     
        DataFactory.createLatestDates(orgID, actualDate, projUpdateDate,invUpdateDate);
       
          List<SKU__c> skuList = DataFactory.createSKUs(40, 3, 'Air Care', 'Active & Planned','All Air Care','Home Care');
         Date projDate = System.today().addMonths(-1).toStartOfMonth();
          List<SKU__c> skuList2 = new List<SKU__c>();
          List<Brand__c> brandList = new List<Brand__c>();
          List<Brand__c> brandList2 = new List<Brand__c>();
          List<Brand__c> brandList3 = new List<Brand__c>();
          Integer count = 0;
          for(Integer r = 0 ; r < skuList.size();r++) {
            if(count < 10) brandList.addAll(skuList[r].Brands__r);
            else if(count >=10 && count <15)  brandList2.addAll(skuList[r].Brands__r);
            else if(count >= 15)  {
                brandList3.addAll(skuList[r].Brands__r);
                skuList2.add(skuList[r]);
                skuList.remove(r);

            }
            count++;      

        }        
        DataFactory.createSKUActual(skuList,20,'Home Care',System.today().addMonths(-1).toStartOfMonth());
        List<Brand_Actual_Staging__c> basList = DataFactory.createBrandActualStaging(200,System.today().addMonths(-1).toStartOfMonth(),'Active & Planned','Home Care');

        RunProcessBatchController runProcess = new RunProcessBatchController();
       // runProcess.ActualsRun = true;
        runProcess.GBU = 'Home Care';
        Test.startTest();
        runProcess.RunActuals();
        runProcess = new RunProcessBatchController();
        Test.stopTest();
        for(Integer i = 1; i < 5; i++) 
        runProcess.PollJobs();   

        System.assert(runProcess.BrandActualsJobs.size() > 0);
        System.assertEquals(false,runProcess.ActualsRun);
        System.assertEquals(false, [Select isJobRunning__c from BatchJobStatus__c where Name='Actuals'].IsJobRunning__c);
       List<BatchStatus__c> bsPostRunCheck = [Select JobName__c,JobOrder__c,JobStatus__c,JobErrors__c,JobStartDate__c,JobEndDate__c from BatchStatus__c where JobType__c='Actuals' order by JobOrder__c ASC];
        for(BatchStatus__c bs:bsPostRunCheck) {
            System.assertEquals('Completed',bs.JobStatus__c);
            System.assertEquals(null,bs.JobErrors__c);
        }
       runProcess.GBU = 'SelectGBU';
        runProcess.RunActuals();
        bsPostRunCheck = [Select JobName__c,JobOrder__c,JobStatus__c,JobErrors__c,JobStartDate__c,JobEndDate__c from BatchStatus__c where JobType__c='Actuals' and JobOrder__c = 1];
        for(BatchStatus__c bs:bsPostRunCheck) {
           
            System.assertEquals('Error',bs.JobStatus__c);
            System.assertEquals('Please select a valid GBU',bs.JobErrors__c);
        }
        System.assertEquals(false, [Select isJobRunning__c from BatchJobStatus__c where Name='Actuals'].IsJobRunning__c);     

    }

    static testmethod void TestBatchProcessInventory() {
        List<BatchStatus__c> bsList = new List<BatchStatus__c>();
        List<BatchJobStatus__c> bjsList = new List<BatchJobStatus__c>();
        bsList.add(new BatchStatus__c(JobOrder__c=1,JobType__c='Inventory',JobName__c='BatchLoadInventory'));
        bsList.add(new BatchStatus__c(JobOrder__c=2,JobType__c='Inventory',JobName__c='BatchSetInventoryUpdateFlag'));
        bsList.add(new BatchStatus__c(JobOrder__c=3,JobType__c='Inventory',JobName__c='BatchUnSetInventoryUpdateFlag'));
        bsList.add(new BatchStatus__c(JobOrder__c=4,JobType__c='Inventory',JobName__c='BatchUpdateInventoryMissing'));
        bsList.add(new BatchStatus__c(JobOrder__c=5,JobType__c='Inventory',JobName__c='BatchIdentifyPlantInvForElim'));
        bsList.add(new BatchStatus__c(JobOrder__c=6,JobType__c='Inventory',JobName__c='BatchUpdateInvStatusToRdyToSteam'));
        bsList.add(new BatchStatus__c(JobOrder__c=7,JobType__c='Inventory',JobName__c='CreateInventoryTasks'));

        bsList.add(new BatchStatus__c(JobOrder__c=1,JobType__c='Actuals',JobName__c='BatchBrandActualStaging'));
        bsList.add(new BatchStatus__c(JobOrder__c=2,JobType__c='Actuals',JobName__c='MarkMissingBrandCodesAsDeleted'));
        bsList.add(new BatchStatus__c(JobOrder__c=3,JobType__c='Actuals',JobName__c='BatchMarkMissingBrandCodesFound'));
        bsList.add(new BatchStatus__c(JobOrder__c=4,JobType__c='Actuals',JobName__c='BatchUpdateSKULatestStatusUpdateDates'));

        bsList.add(new BatchStatus__c(JobOrder__c=1,JobType__c='Projections',JobName__c='BatchBrandProjectionStaging'));

        insert bsList;

        bjsList.add(new BatchJobStatus__c(Name='Inventory',	isJobRunning__c=false));
        bjsList.add(new BatchJobStatus__c(Name='Actuals',	isJobRunning__c=false));
        bjsList.add(new BatchJobStatus__c(Name='Projections',	isJobRunning__c=false));

        insert bjsList;

          Id orgId = [Select id from Organization].Id;
        Date actualDate = System.today().addMonths(-1).toStartOfMonth();
        Date projUpdateDate = System.today().addMonths(-1).toStartOfMonth();
        Date invUpdateDate = System.today().addMonths(-2).toStartOfMonth();
        Date uploadDate = System.today().addMonths(-1).toStartOfMonth();
        List<Brand__c> brandList = new List<Brand__c>();
        List<Brand__c> brandList2 = new List<Brand__c>();
        
        DataFactory.createLatestDates(orgID, actualDate, projUpdateDate,invUpdateDate);
        DataFactory.createLatestDates_OralCare(orgID, actualDate, projUpdateDate,invUpdateDate);
        List<SKU__c> skuList = DataFactory.createSKUs(20, 3, 'Air Care', 'Active & Planned','All Air Care','Home Care');
        Integer count = 0;
        for(SKU__c s:skuList) {
            if(count < 10) brandList.addAll(s.Brands__r);
            else  brandList2.addAll(s.Brands__r);
            count++;           
        }
        

        List<Plant__c> plantList= DataFactory.createPlant(5);

        DataFactory.CreateInventory(brandList, plantList,3, uploadDate,'Home Care','',true);
        DataFactory.createInventoryStaging(20,uploadDate,'Home Care');
        DataFactory.CreateInventory(brandList2, plantList,3, uploadDate,'Home Care','Ready For Steaming',false);

        RunProcessBatchController runProcess = new RunProcessBatchController();
       // runProcess.InventoryRun = true;
        runProcess.GBU = 'Home Care';
        Test.startTest();
        runProcess.RunInventory();
        runProcess = new RunProcessBatchController();
         Test.stopTest();
        for(Integer i = 1; i < 8; i++) 
        runProcess.PollJobs();       
       
        System.assert(runProcess.InventoryJobs.size() > 0);
        System.assertEquals(false,runProcess.InventoryRun);
        System.assertEquals(false, [Select isJobRunning__c from BatchJobStatus__c where Name='Inventory'].IsJobRunning__c);
        List<BatchStatus__c> bsPostRunCheck = [Select JobName__c,JobOrder__c,JobStatus__c,JobErrors__c,JobStartDate__c,JobEndDate__c from BatchStatus__c where JobType__c='Inventory' order by JobOrder__c ASC];
        for(BatchStatus__c bs:bsPostRunCheck) {
            System.assertEquals('Completed',bs.JobStatus__c);
            System.assertEquals(null,bs.JobErrors__c);
        }
        runProcess.GBU = 'SelectGBU';
        runProcess.RunInventory();
        bsPostRunCheck = [Select JobName__c,JobOrder__c,JobStatus__c,JobErrors__c,JobStartDate__c,JobEndDate__c from BatchStatus__c where JobType__c='Inventory' and JobOrder__c = 1];
        for(BatchStatus__c bs:bsPostRunCheck) {
           
            System.assertEquals('Error',bs.JobStatus__c);
            System.assertEquals('Please select a valid GBU',bs.JobErrors__c);
        }
        System.assertEquals(false, [Select isJobRunning__c from BatchJobStatus__c where Name='Inventory'].IsJobRunning__c);

     

      
    }
}