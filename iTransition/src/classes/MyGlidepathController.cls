public class MyGlidepathController {
	
	public class dataHeaderWrapper{
        //@AuraEnabled public list<integer> category;
        //@AuraEnabled public list<integer> AP;
        @AuraEnabled public string TableRowHeading;
        @AuraEnabled public string CategoryName;
        @AuraEnabled public list<integer> TableRowValues;        
        @AuraEnabled public list<String> Tableheadings;
        
        public dataHeaderWrapper(string CategoryName, string TableRowHeading, list<integer> TableRowValues, list<String> Tableheadings){
            
            this.CategoryName = CategoryName;
            this.TableRowHeading = TableRowHeading;
            this.TableRowValues = TableRowValues;
            this.Tableheadings = Tableheadings;
            //this.headings = headings;
        }
    }
    
    public class excelWrapper{
        @AuraEnabled public document template;
        @AuraEnabled public list<ExportToExcel__c> ex2ex;        
        
        public excelWrapper(document template, list<ExportToExcel__c> ex2ex){
            this.template = template;
            this.ex2ex = ex2ex;
        }
    }  

    @AuraEnabled public static map<string, list<dataHeaderWrapper>> ActualsResult(list<string> category, list<string> lifecycle, list<string> brand, list<string> Stype,string Sdate,string Edate, string userGBU){ 
    	
    	
    	system.debug('category==='+category);
        system.debug('lifecycle===='+lifecycle);
        system.debug('brand===='+brand);
        system.debug('Stype===='+Stype);
        system.debug('userGBU===='+userGBU);
        
    	if(userGBU==null || userGBU==''){
    		userGBU='Home Care';
    		category=new list<string>{'Air Care'};
    		brand=new list<string>{'All Air Care'};
    	}
    		 //userGBU.isEmpty()
        /*	
        if(category.isEmpty()){
            
                // category=new list<string>{'Air Care','Dish Care','Surface Care'};
                }
        if(brand.isEmpty()){
            
                //  brand=new list<string>{'All Air Care','ADW','HDW','Mr Clean','Swiffer'};
                }
        */
        if(Stype.isEmpty()){
            Stype=new list<string>{'GBU'};
                }
        if(lifecycle.isEmpty()){
            lifecycle=new list<string>{'Active & Planned'};
                }
		//category.sort();
        //date ActLatestDate = LatestDates__c.getOrgDefaults().Date__c;
                
        date startMonth;
        date endMonth;
        date fstartMonth;
        date fendMonth; 
        
        if(sDate == '' || Edate == '' || sDate == null || Edate == null ){
        	sDate = string.valueOf(system.today().addMonths(-13));
        	Edate = string.valueOf(system.today().addMonths(-1));
        }       
                
        system.debug('--Sdate---'+Sdate+'---Edate---'+Edate);
        Date Sdate1 = date.valueOf(sDate);
        	Sdate1 = date.newInstance(Sdate1.year(),Sdate1.month(),3);
        Date Edate1 = date.valueOf(Edate);
        	Edate1 = date.newInstance(Edate1.year(),Edate1.month(),3);
        Date Tdate1 = LatestDates__c.getOrgDefaults().Date__c;   			//ActLatestDate
        	Tdate1 = date.newInstance(Tdate1.year(),Tdate1.month(),3);
        
        system.debug('---Sdate1---'+Sdate1+'---Edate1---'+Edate1);
        
        if(Sdate1 > Edate1){
        	startMonth = Tdate1.addMonths(-11);
        	endMonth = Tdate1;
        	fstartMonth = Tdate1.addMonths(1);
        	fendMonth = Tdate1.addMonths(12);
        }
        else if(Edate1 <= Tdate1){
        	startMonth = Sdate1;	//Tdate1.addMonths(-11);		// Change here if start date should be the start of financial year
        	endMonth = Edate1;
        	fstartMonth = Tdate1.addMonths(1);
        	fendMonth = Tdate1.addMonths(12);
        	
        	system.debug('---startMonth---'+startMonth+'---endMonth---'+endMonth);
        }
        else if(Sdate1 > Tdate1 && Edate1 > Tdate1){        	
        	startMonth = Tdate1.addMonths(-11);
        	endMonth = Tdate1;
        	fstartMonth = Sdate1;
        	fendMonth = Edate1;
        }
        else if(Edate1 > Tdate1){        	
        	startMonth = Tdate1.addMonths(-11);
        	if(Sdate1 > Tdate1.addMonths(-11))
				startMonth = Sdate1;
        	endMonth = Tdate1;
        	fstartMonth = Tdate1.addMonths(1);
        	fendMonth = Edate1;
        }
        
        //system.debug(Sdate1 < date.newInstance(2016,7,1));
        
        if(startMonth < date.newInstance(2016,7,1)){
        	startMonth = date.newInstance(2016,7,1);
        }
        
        system.debug('---startMonth---'+startMonth+'---endMonth---'+endMonth);
        
        //Building Headers ------------------------

        //date startMonth=date.newInstance(2016,6,1);
        //date endMonth=date.newInstance(2017,7,1);
        
        list<String> dateHeadings = new list<String>();
        
        while(startMonth <= endMonth)
        {
            String strDate = startMonth+'';
            DateTime dt = DateTime.valueOf(strDate);
            //strDate = dt.format('MMMMM YYYY');
            strDate = dt.format('MMM-YYYY');
            //system.debug('strDate==='+strDate);
            startMonth = startMonth.AddMonths(1);
            dateHeadings.add(strDate);
        }
        system.debug('dateHeadings---------'+dateHeadings);
        //Building Future Table Headers ------------------------
        
        //date fstartMonth=date.newInstance(statyear,statmon,1);	//fixed
        //date fendMonth=date.newInstance(2018,12,1);
        
        list<String> futDateHeadings = new list<String>();
        
        while(fstartMonth <= fendMonth)
        {
            String strDate = fstartMonth+'';
            DateTime dt1 = DateTime.valueOf(strDate);
            strDate = dt1.format('MMM-YYYY');
            //system.debug('strDate==='+strDate);
            fstartMonth = fstartMonth.AddMonths(1);
            futDateHeadings.add(strDate);
        }
        
        //system.debug('futDateHeadings---------'+futDateHeadings);
        

    	map<string, list<dataHeaderWrapper>> tableResul = new map<string, list<dataHeaderWrapper>>();
    	//list<string> category = new list<string>{'Air Care','Dish Care','Surface Care'}; 
        //list<string> lifecycle = new list<string>{'Active & Planned','Remnant & Historical'};
                
        map<integer,string> calMap =  new map<integer,string>{ 1=>'Jan',2=>'Feb', 3=>'Mar', 4=>'Apr',5=>'May', 6=>'Jun',
                                             7=>'Jul', 8=>'Aug',9=>'Sep', 10=>'Oct', 11=>'Nov', 12=>'Dec'};
        
        AggregateResult[] MonActls = [SELECT SKU_Category__c, Actual_Status__c, CALENDAR_Year(Date__c)year, CALENDAR_month(Date__c)month, count(id)TotalSkus 
        						FROM SKU_Actual__c where SKU__r.GBU__c=:userGBU and 
								SKU__r.Dish_Care_Group__c in :brand and 
								SKU__r.SKU_Type__c in :Stype and 
                                SKU_Category__c=:category  
        						group by SKU_Category__c, Actual_Status__c, CALENDAR_Year(Date__c), CALENDAR_month(Date__c) 
        						order by SKU_Category__c, CALENDAR_Year(Date__c), CALENDAR_month(Date__c)]; 
        
        Map<string,integer> MonActuals = new map<string,integer>();
        for(AggregateResult a: MonActls){        	
        
        	string Acategory = (string)a.get('SKU_Category__c');
    		string AStatus = (string)a.get('Actual_Status__c');
    		integer Ayear = (integer)a.get('year');
    		string Amonth = calMap.get((integer)a.get('month'));
    		integer cont = (integer)a.get('TotalSkus');
    		//system.debug(Acategory+'--'+AStatus+'--'+Amonth+'--'+Ayear+'--'+cont);
    		if( Acategory!=null && Acategory!='' && AStatus!=null && AStatus!='' && Ayear!=0 && Amonth!=null ){
    			MonActuals.put(Acategory+'~'+AStatus+'~'+Amonth+'-'+Ayear,cont);
        	}
        }
        
        Map<string,integer> MonActuals4Proj = new map<string,integer>();
        for(AggregateResult a: MonActls){        	
        
        	string Acategory = (string)a.get('SKU_Category__c');
    		string AStatus = (string)a.get('Actual_Status__c');
    		integer Ayear = (integer)a.get('year');
    		integer mon = (integer)a.get('month') + 1;
    		if(mon == 13){
    			mon=1;
    			Ayear += 1;
    		}
    		string Amonth = calMap.get(mon);    	
    		integer cont = (integer)a.get('TotalSkus');
    		//system.debug(Acategory+'--'+AStatus+'--'+Amonth+'--'+Ayear+'--'+cont);
    		if( Acategory!=null && Acategory!='' && AStatus!=null && AStatus!='' && Ayear!=0 && Amonth!=null ){
    			MonActuals4Proj.put(Acategory+'~'+AStatus+'~'+Amonth+'-'+Ayear,cont);
        	}
        }
        //system.debug(MonActuals4Proj);
        
        AggregateResult[] projecCount= [Select SKU__r.Category__c, Projection_Type__c,CALENDAR_Year(Projected_date__c)year, CALENDAR_month(Projected_date__c)month, 
										count(id)Projections from sku_Projection__c where SKU__r.GBU__c=:userGBU 
										and Include_in_Projections__c=true 
										and SKU__r.Dish_Care_Group__c in :Brand and SKU__r.SKU_Type__c in :Stype
										GROUP BY SKU__r.Category__c, Projection_Type__c,CALENDAR_Year(Projected_date__c), CALENDAR_month(Projected_date__c)];
        Map<string,integer> Projections = new map<string,integer>();
        for(AggregateResult a: projecCount){
        	        
        	string Pcategory = (string)a.get('Category__c');
    		string Ptype = (string)a.get('Projection_Type__c');
    		integer Pyear = (integer)a.get('year');
    		string Pmonth = calMap.get((integer)a.get('month'));
    		integer cont = (integer)a.get('Projections');
    		//system.debug(Pcategory+'--'+Ptype+'--'+Pmonth+'--'+Pyear+'--'+cont);
    		if( Pcategory!=null && Pcategory!='' && Ptype!=null && Ptype!='' && Pyear!=0 && Pmonth!=null ){
    			Projections.put(Pcategory+'~'+Ptype+'~'+Pmonth+'-'+Pyear,cont);
        	}
        }
        //system.debug(Projections);
        
            list<Historical_Projection_Counts__c> lstHPC =[SELECT Category__c, Count__c, Date_of_Projection__c, Projection_Type__c 
        												FROM Historical_Projection_Counts__c WHERE Category__c in :category ];
        
        //system.debug('lstHPC===='+lstHPC);
        for(Historical_Projection_Counts__c ar: lstHPC){
        	if(ar.Date_of_Projection__c != null ){
	            Integer Pyear = ar.Date_of_Projection__c.year();  
	            string Pmonth = calMap.get(ar.Date_of_Projection__c.month());
	            string Pcategory = ar.Category__c;
	    		string Ptype = ar.Projection_Type__c;
	    		integer cont = ar.Count__c.intValue();
	    		if( Pcategory!=null && Pcategory!='' && Ptype!=null && Ptype!='' && Pyear!=0 && Pmonth!=null ){
	    			Projections.put(Pcategory+'~'+Ptype+'~'+Pmonth+'-'+Pyear,cont);
	        	}    
        	}        
        }
        //system.debug(Projections.size());
        
        //system.debug(MonActuals);
        

        //system.debug('dateHeadings---------'+dateHeadings);
        //system.debug(MonActuals.keyset());
        
        list<dataHeaderWrapper> actList = new list<dataHeaderWrapper>();
        for(string cate: category){
        	
        	list<integer> ap = new list<integer>();
	        list<integer> rh = new list<integer>();
	        list<integer> totalActals = new list<integer>();
	        
        	for(string heading : dateHeadings){
        		
        		integer totalActal=0;
        		
        		for(string lc: new list<string>{'Active & Planned','Remnant & Historical'}){
        			integer actualValue = 0;
    				if(!MonActuals.isEmpty() && MonActuals.containsKey(cate+'~'+lc+'~'+heading))
    					actualValue = MonActuals.get(cate+'~'+lc+'~'+heading);    				
    				
        			if(lc=='Active & Planned')        				
        				ap.add(actualValue);        			
        			else if(lc=='Remnant & Historical')
        				rh.add(actualValue);

        			totalActal += actualValue;
        		}
        		totalActals.add(totalActal);
        		
        	}
        	
        	actList.add(new dataHeaderWrapper(cate,cate,totalActals,dateHeadings));
        	
        	for(string lc:lifecycle){
	        	if(lc=='Active & Planned')	
	        	actList.add(new dataHeaderWrapper(cate,'A & P',ap,dateHeadings));
	        	else if(lc=='Remnant & Historical')
	        	actList.add(new dataHeaderWrapper(cate,'R & H',rh,dateHeadings));
        	}
        }
        
        //system.debug(actList);
        tableResul.put('Actuals',actList);
        
        list<dataHeaderWrapper> projList = new list<dataHeaderWrapper>();
        for(string cate: category){
        	
        	list<integer> ap = new list<integer>();
	        list<integer> rh = new list<integer>();
	        list<integer> monProjs = new list<integer>();
	        
        	for(string heading : dateHeadings){
        		integer totalProjections=0;
        		
        		for(string lc: new list<string>{'Active & Planned','Remnant & Historical'}){
        			integer projValue = 0;
        			integer newValue = 0;
        			integer discoValue = 0;
        			
        			if(!Projections.isEmpty() && Projections.containsKey(cate+'~'+'New'+'~'+heading))
    					newValue = Projections.get(cate+'~'+'New'+'~'+heading);
    				if(!Projections.isEmpty() && Projections.containsKey(cate+'~'+'Disco'+'~'+heading))
    					discoValue = Projections.get(cate+'~'+'Disco'+'~'+heading);
        			if(!MonActuals4Proj.isEmpty() && MonActuals4Proj.containsKey(cate+'~'+lc+'~'+heading))
    					projValue = MonActuals4Proj.get(cate+'~'+lc+'~'+heading) ;        					        			
        			//system.debug('newValue---'+newValue+'discoValue--'+discoValue+'projValue--'+projValue);	
        			if(lc=='Active & Planned'){
        				//system.debug(cate+'~'+lc+'~'+heading);        				       				        				
        				projValue = projValue + newValue - discoValue;
        				ap.add(projValue);
        			}
        			else if(lc=='Remnant & Historical'){        				       				
        				projValue += discoValue;
        				rh.add(projValue);
        			}
        			totalProjections += projValue ;
        			//system.debug('newValue---'+newValue+'discoValue--'+discoValue+'projValue--'+projValue);
        		}
        		monProjs.add(totalProjections);        		
        	}
        	projList.add(new dataHeaderWrapper(cate,cate,monProjs,dateHeadings));
        	
        	for(string lc:lifecycle){
	        	if(lc=='Active & Planned')	
	        	projList.add(new dataHeaderWrapper(cate,'A & P',ap,dateHeadings));
	        	else if(lc=='Remnant & Historical')
	        	projList.add(new dataHeaderWrapper(cate,'R & H',rh,dateHeadings));
        	}
        }
        //system.debug(projList);
        tableResul.put('PastProjections',projList);
        
        //--------------------------------------------------        
        
        AggregateResult[] baseActls = [SELECT SKU_Category__c, Actual_Status__c,date__c,count(id)Total 
                                                FROM SKU_Actual__c 
                                                where SKU__r.GBU__c='Home care' 
                                                and Is_Latest_Actual__c=true 
                                            	and SKU__r.IsSKUMissing__c=false 
												and SKU__r.Dish_Care_Group__c in :brand  
												and SKU__r.SKU_Type__c in :Stype  
				                                and SKU_Category__c=:category       
                                                group by SKU_Category__c, Actual_Status__c,date__c]; 
        
        Map<string,integer> baseActlsMap = new map<string,integer>();
        integer statmon = 0;
        integer statyear = 0;
        
        for(AggregateResult a: baseActls){        	
        
        	statmon = ((date)a.get('date__c')).month();
        	statyear =((date)a.get('date__c')).year();
        	
        	string Acategory = (string)a.get('SKU_Category__c');
    		string AStatus = (string)a.get('Actual_Status__c');
    		integer cont = (integer)a.get('Total');
    		if( Acategory!=null && Acategory!='' && AStatus!=null && AStatus!=''){
    			baseActlsMap.put(Acategory+'~'+AStatus,cont);
        	}
        }               
        
        list<dataHeaderWrapper> futureList = new list<dataHeaderWrapper>();
        for(string cate: category){
        	
        	list<integer> ap = new list<integer>();
	        list<integer> rh = new list<integer>();
	        list<integer> futProjs = new list<integer>();
	        
	        integer catActiveBase = 0;
	        integer catRemnantBase = 0;
	        
	        if(!baseActlsMap.isEmpty() && baseActlsMap.containsKey(cate+'~'+'Active & Planned'))
	        	catActiveBase = baseActlsMap.get(cate+'~'+'Active & Planned');
	        if(!baseActlsMap.isEmpty() && baseActlsMap.containsKey(cate+'~'+'Remnant & Historical'))
	        	catRemnantBase = baseActlsMap.get(cate+'~'+'Remnant & Historical');
	        	
	        	
        	for(string heading : futDateHeadings){
        		integer totalProjections=0;
        		
        		for(string lc: new list<string>{'Active & Planned','Remnant & Historical'}){
        			integer projValue = 0;
        			integer newValue = 0;
        			integer discoValue = 0;
        			
        			if(!Projections.isEmpty() && Projections.containsKey(cate+'~'+'New'+'~'+heading))
    					newValue = Projections.get(cate+'~'+'New'+'~'+heading);
    				if(!Projections.isEmpty() && Projections.containsKey(cate+'~'+'Disco'+'~'+heading))
    					discoValue = Projections.get(cate+'~'+'Disco'+'~'+heading);
        			if(lc=='Active & Planned'){
        				projValue = catActiveBase + newValue - discoValue;
        				catActiveBase = projValue;
        				ap.add(projValue);
        			}
        			else if(lc=='Remnant & Historical'){        					       				
        				projValue = catRemnantBase + discoValue;
        				catRemnantBase = projValue;
        				rh.add(projValue);
        			}
        			totalProjections += projValue ;
        			//system.debug('newValue---'+newValue+'discoValue--'+discoValue+'projValue--'+projValue);
        		}
        		futProjs.add(totalProjections);        		
        	}
        	futureList.add(new dataHeaderWrapper(cate,cate,futProjs,futDateHeadings));
        	
        	for(string lc:lifecycle){
	        	if(lc=='Active & Planned')	
	        	futureList.add(new dataHeaderWrapper(cate,'A & P',ap,futDateHeadings));
	        	else if(lc=='Remnant & Historical')
	        	futureList.add(new dataHeaderWrapper(cate,'R & H',rh,futDateHeadings));
        	}
        }
        //system.debug(futureList);
        tableResul.put('FutureProjections',futureList);
   
   return tableResul; 
}
	@AuraEnabled public static List<Brand__c> fetchSKUdetails(list<string> category, list<string> lifecycle, list<string> brand, list<string> Stype, string userGBU, string mon_year ){
		
		
		if(userGBU==null || userGBU=='') //userGBU.isEmpty()
        	userGBU='Home Care';
        	
		category.sort();
		system.debug('category==='+category);
        system.debug('lifecycle===='+lifecycle);
        system.debug('brand===='+brand);
        system.debug('Stype===='+Stype);
        system.debug('userGBU===='+userGBU);
        
        if(mon_year=='' || mon_year == null){
        	mon_year='Jan-2000';
        }
        String[] arrMon = mon_year.split('-');
        //system.debug('arrMon[0]===='+arrMon[0]+'--arrMon[1]----'+arrMon[1]);
        
		map<string,string> calMap =  new map<string,string>{'Jan'=>'1','Feb'=>'2', 'Mar'=>'3', 'Apr'=>'4','May'=>'5', 'Jun'=>'6',
                                             'Jul'=>'7', 'Aug'=>'8','Sep'=>'9', 'Oct'=>'10', 'Nov'=>'11', 'Dec'=>'12'};
        
        integer month1;
        if(calMap.containsKey(arrMon[0]))                                     
        month1 = Integer.valueOf(calMap.get(arrMon[0]));
        
        integer year1 = Integer.valueOf(arrMon[1]);
        //system.debug('month1---'+month1+'--year1----'+year1);
        list<SKU_Actual__c> SKUidlist = new list<SKU_Actual__c>();
         
        if(calMap.containsKey(arrMon[0])){
        SKUidlist = [SELECT Id , SKU__c FROM SKU_Actual__c where SKU__r.GBU__c=:userGBU AND 
                                SKU_Category__c=:category AND 
								SKU__r.Dish_Care_Group__c in :brand AND 
								SKU__r.SKU_Type__c in :Stype AND 
								Actual_Status__c = :lifecycle AND 
								CALENDAR_MONTH(Date__c) = :month1 AND
								CALENDAR_YEAR(Date__c) = :year1];
        }
								
		set<id> skuids = new set<id>();								
		for(SKU_Actual__c s: SKUidlist){
			skuids.add(s.SKU__c);
		}
								
		list<Brand__c> brandDetails = [Select b.Type__c, b.Total_EOH_Inventory__c, b.SKU__c,SKU__r.SKU__c, b.Id, b.Brand_Projection_Type__c, 
										b.Brand_Description__c, b.Brand_Code__c, Net_Invoice_Value_MM__c,Shipments_in_MSU__c, 
										Three_Month_MM_Net_Invoice__c, Three_Month_MSU_Shipment__c,EOH_Inventory__c,Total_Quantity_Actuals__c  
                    					From Brand__c b
                    					WHERE SKU__c IN :skuids];
		
		list<Brand__c> brandDetails1 = [select id from Brand__c];
		system.debug('brandDetails1======='+brandDetails1.size());
		system.debug('brandDetails======='+brandDetails);
        
        return brandDetails;
        
	}
	
	@AuraEnabled public static list<ExportToExcel__c> exportToExcelSettings(){
        
       	excelWrapper ExcelWrap; // = new excelWrapper();
        
        list<ExportToExcel__c> lstExports = [SELECT Name, Required__c from ExportToExcel__c ];
        //Map<String,ExportToExcel__c> allExports = ExportToExcel__c.getAll();
        //lstExports = allExports.values();
        
        Document doc;
		//doc = [select id, name, body from Document where name = 'test' limit 1];
        system.debug(doc);
        
        ExcelWrap = new excelWrapper(doc,lstExports);
        system.debug(ExcelWrap);
        
        return lstExports;
    }
     

}