public class RunProcessBatchController { 
    /*public List<BatchStatus__c> InventoryJobs {get;set;}
    public List<BatchStatus__c> BrandActualsJobs {get;set;}
    public List<BatchStatus__c> BrandProjectionsJobs {get;set;}
*/
    public List<RunProcessBatchController.Batchinfo> InventoryJobs {get;set;}
    public List<RunProcessBatchController.Batchinfo> BrandActualsJobs {get;set;}
    public List<RunProcessBatchController.Batchinfo> BrandProjectionsJobs {get;set;}
  
  
    public Boolean InventoryRun {get;set;}
    public Boolean ActualsRun {get;set;}
    public Boolean ProjectionsRun {get;set;}
    public Boolean canPoll {get;set;}
    public Boolean deleteJobs {get;set;}
    public Integer deletedJobCount {get;set;}

    public Boolean InventoryErrors {get;set;}
 
    public String GBU {get;set;}

 
    private DateTime startDate ;

    public class BatchInfo {
        public String  jobName {get;set;}
        public Integer jobOrder {get;set;}
        public String  jobType {get;set;}
        public Integer totalJobs {get;set;}
        public Integer jobsProcessed {get;set;}
        public Integer failures {get;set;}
        public String  submitDate {get;set;}
        public String  completeDate {get;set;}
        public String  jobErrors {get;set;}
        public String  jobStatus {get;set;}
    }

    class InvalidGBUException extends Exception{}
    public RunProcessBatchController() {
        System.Debug('In Constructor');
        Init();  
    }

    private Boolean isJobRunning(String jobType) {
        return [Select isJobRunning__c from  BatchJobStatus__c where Name = :jobType ].isJobRunning__c;
    }
    private void Init() {
     
        InventoryRun = isJobRunning('Inventory');
        ActualsRun = isJobRunning('Actuals');  
        ProjectionsRun = isJobRunning('Projections'); 
          System.Debug('in Init'+InventoryRun+' '+ActualsRun+' '+ProjectionsRun); 
        canPoll = InventoryRun ||  ActualsRun || ProjectionsRun;
        deleteJobs = false;
        inventoryErrors = false;
       
        startDate = System.now();
       
        if(!InventoryRun) {
         InventoryJobs = new List<RunProcessBatchController.Batchinfo>();
         InventoryJobs = GetWrapperData(GetJobData('Inventory'));         
        }
        else if(InventoryRun) {
          String jsonDataInv = (String)cache.session.get('InvbatchData');
          if(jsonDataInv !=null && jsonDataInv !='') 
           InventoryJobs = (List<RunProcessBatchController.Batchinfo>)JSON.deserialize(jsonDataInv,List<RunProcessBatchController.Batchinfo>.class);
        }

        if(!ActualsRun) {
          BrandActualsJobs = new List<RunProcessBatchController.Batchinfo>();
            BrandActualsJobs = GetWrapperData(GetJobData('Actuals'));
        }
        else if(ActualsRun) {
          String jsonDataActuals = (String)cache.session.get('ActualsbatchData');
          if(jsonDataActuals !=null && jsonDataActuals !='') 
           BrandActualsJobs = (List<RunProcessBatchController.Batchinfo>)JSON.deserialize(jsonDataActuals,List<RunProcessBatchController.Batchinfo>.class);
        }

        if(!ProjectionsRun) {
             BrandProjectionsJobs = new List<RunProcessBatchController.Batchinfo>();
              BrandProjectionsJobs = GetWrapperData(GetJobData('Projections'));
        }
        else if(ProjectionsRun) {
           String jsonDataProjections = (String)cache.session.get('ProjectionsbatchData');
          if(jsonDataProjections !=null && jsonDataProjections !='') 
           BrandProjectionsJobs = (List<RunProcessBatchController.Batchinfo>)JSON.deserialize(jsonDataProjections,List<RunProcessBatchController.Batchinfo>.class);
        }
       
    }

    private List<RunProcessBatchController.Batchinfo> GetWrapperData(List<BatchStatus__c> jobData) {
        List<RunProcessBatchController.Batchinfo> wrapperList = new List<RunProcessBatchController.Batchinfo>();
        for(BatchStatus__c job:jobData) {
            RunProcessBatchController.Batchinfo obj = new RunProcessBatchController.Batchinfo();
            obj.jobName = job.jobName__c;
            obj.jobOrder = Integer.valueOf(job.jobOrder__c);
            obj.jobType = job.jobType__c;
            obj.submitDate = job.jobStartDate__c != null ? job.jobStartDate__c.format():null;
            obj.completeDate = job.JobEndDate__c != null ? job.JobEndDate__c.format():null;
            obj.jobStatus = job.JobStatus__c;
            obj.jobErrors = job.JobErrors__c;
            wrapperList.add(obj);
        }
        return wrapperList;
    }

    private List<BatchStatus__c> GetJobData(String JobType) {
        return [Select JobName__c,JobOrder__c,JobStatus__c,JobType__c,JobStartDate__c,JobEndDate__c,JobErrors__c from BatchStatus__c where JobType__c = :JobType order by JobType__c,JobOrder__c ASC ];
    }

    private void UpdateRunningJobStatus(String jobType,Boolean status) {
         BatchJobStatus__c bjs = [Select isJobRunning__c from BatchJobStatus__c where Name= :jobType LIMIT 1];
         bjs.isJobRunning__c = status;
         update bjs;
    }

    /*  public PageReference clear() {
        try {        
          Cache.Session.remove('InvbatchData');       
        }catch(Exception ex) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage()+' '+ex.getstackTraceString()));
        }
        return null;
    } */

    public PageReference RunProjections() {
        System.Debug('GBU = '+GBU);
        canPoll = true;   
        BatchStatus__c bs ;    
        try {
           bs = UpdateBatchStateInfo('Projections');  
           if(GBU== 'SelectGBU') throw new  InvalidGBUException('Please select a valid GBU'); 
            ProjectionsRun = true;
            UpdateRunningJobStatus('Projections',true);                 
            BrandProjectionsJobs = GetWrapperData(GetJobData('Projections'));
            Id jobId = Database.executeBatch(new BatchBrandProjectionStaging(Enums.getGBUEnum(GBU)));
            cache.Session.put('ProjectionsbatchData',JSON.serialize(BrandProjectionsJobs));
       
         }catch(Exception ex) {
             canPoll = false;     
             //if(!ex.getMessage().contains('Please select a valid GBU')) {
                bs.JobErrors__c = ex.getMessage();
                bs.JobStatus__c = 'Error';
                update bs;
              //}       
           
             UpdateRunningJobStatus('Projections',false);               
             ProjectionsRun = false;    
             BrandProjectionsJobs = GetWrapperData(GetJobData('Projections'));                 
             Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage())); 	 
         }     
        return null;
    }

    public PageReference RunActuals() {
          System.Debug('GBU = '+GBU);
          canPoll = true;   
          BatchStatus__c bs ;        
          try {
            bs = UpdateBatchStateInfo('Actuals');        
            if(GBU== 'SelectGBU') throw new  InvalidGBUException('Please select a valid GBU');   
            ActualsRun = true;   
            UpdateRunningJobStatus('Actuals',true);           
            BrandActualsJobs = GetWrapperData(GetJobData('Actuals'));
            Id jobId = Database.executeBatch(new BatchBrandActualStaging(Enums.getGBUEnum(GBU)));
              cache.Session.put('ActualsbatchData',JSON.serialize(BrandActualsJobs));
         }catch(Exception ex) {
             canPoll = false;     
             //if(!ex.getMessage().contains('Please select a valid GBU')) {
                bs.JobErrors__c = ex.getMessage();
                bs.JobStatus__c = 'Error';
                update bs;
             // }       
           
             UpdateRunningJobStatus('Actuals',false);               
             ActualsRun = false;    
             BrandActualsJobs = GetWrapperData(GetJobData('Actuals'));                 
             Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage())); 	 
         } 
        return null;
    }

    public PageReference RunInventory() {
        
         canPoll = true;      
         System.Debug('GBU = '+GBU); 
         BatchStatus__c bs ;
         try {
           bs = UpdateBatchStateInfo('Inventory');     
          if(GBU== 'SelectGBU') throw new  InvalidGBUException('Please select a valid GBU'); 
          InventoryRun = true;  
          UpdateRunningJobStatus('Inventory',true); 
             
          InventoryJobs = GetWrapperData(GetJobData('Inventory'));
          Id jobId = Database.executeBatch(new BatchLoadInventory(Enums.getGBUEnum(GBU)));
           cache.Session.put('InvbatchData',JSON.serialize(InventoryJobs));
         }catch(Exception ex) {
             System.Debug('Encountered exception '+ex.getMessage());
            // if(InventoryRun) InventoryRun = false;
             canPoll = false;
            // if(!ex.getMessage().contains('Please select a valid GBU')) {
                 bs.JobErrors__c = ex.getMessage();
                 bs.JobStatus__c = 'Error';
                 update bs;                              
             //}        
             UpdateRunningJobStatus('Inventory',false);    
             InventoryRun = false;    
             InventoryJobs = GetWrapperData(GetJobData('Inventory'));   
             Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage())); 	 
         } 
        return null;
    }

   /*  public PageReference DeleteOldJobs() {
        Integer count = System.purgeOldAsyncJobs(Date.today().addDays(1));
        System.debug('Deleted ' + count + ' old jobs.');
        deleteJobs = true;
        deletedJobCount = count; 
        return null;
    }  */

    private void UpdateWrapperJobStatus(String jobName,Integer order,String Status,String jobType) {

        if(jobType == 'Inventory') {
          for(RunProcessBatchController.Batchinfo invbi:InventoryJobs) {
             if(invbi.jobName == jobName && invbi.jobOrder == order) {
                  invbi.jobStatus = Status;
             }
           }
        }
        else if(jobType == 'Actuals') {            
          for(RunProcessBatchController.Batchinfo invbi:BrandActualsJobs) {
             if(invbi.jobName == jobName && invbi.jobOrder == order) {
                  invbi.jobStatus = Status;
             }
           }
        }
        
    }

    private BatchStatus__c UpdateBatchStateInfo(String jobType) {
          BatchStatus__c bs = [Select JobStatus__c,JobErrors__c,JobStartDate__c from BatchStatus__c where JobOrder__c = 1 and JobType__c = :jobType];
          bs.JobStatus__c =  'In Process';
          bs.JobStartDate__c = DateTime.parse(startDate.format());
          bs.JobErrors__c = null;
          update bs; 
          return bs;
    }

  private void pollProjections() {
       String jobName = '';
       Integer jobOrder = 0;
       BatchStatus__c bsUpdate,bsUpdateNext ;
       AsyncApexJob job;
       Integer maxOrder = 0;
          try {
              System.Debug('poll Projections ' +BrandProjectionsJobs);
                  maxOrder = GetMaxOrder('Projections');
            for(RunProcessBatchController.Batchinfo invbi: BrandProjectionsJobs ){
                    if(invbi.jobStatus == 'In Process' && invbi.jobType == 'Projections') {
                        jobName = invbi.jobName;
                        jobOrder = invbi.jobOrder;
                        break;
                    }
                }
                System.Debug('fetch status for '+jobName+' '+jobOrder);
                if(JobName == '') return ;
                job = [SELECT Id,ExtendedStatus, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems,CreatedBy.Email,JobType,ApexClass.Name,CreatedDate,CompletedDate
                                            FROM AsyncApexJob  
                                    where JobType='BatchApex' and ApexClass.Name = :jobName and CreatedDate >= :System.today() ORDER BY CreatedDate DESC LIMIT 1];
                System.Debug('job details '+job);
                if(job != null){
                        for(RunProcessBatchController.Batchinfo invbi: BrandProjectionsJobs ) {
                            if(invbi.jobName == job.ApexClass.Name) {
                                invbi.jobsProcessed = job.JobItemsProcessed;
                                invbi.totalJobs = job.TotalJobItems;
                                invbi.failures = job.NumberOfErrors; 
                                invbi.submitDate = job.CreatedDate !=null ? job.CreatedDate.format() : null;
                                invbi.completeDate = job.CompletedDate != null ? job.CompletedDate.format():null;                        
                                if(job.status == 'Completed' && invbi.jobStatus == 'In Process') {
                                    System.Debug('update status to completed '+jobOrder);
                                    bsUpdate = [Select JobStatus__c,JobErrors__c from BatchStatus__c where JobName__c=:invbi.jobName];
                                    invbi.failures = job.NumberOfErrors;
                                    invbi.jobsProcessed = job.JobItemsProcessed;
                                    invbi.jobErrors = job.ExtendedStatus;
                                    bsUpdate.JobStatus__c = job.Status;
                                    bsUpdate.JobErrors__c = job.ExtendedStatus;
                                    bsUpdate.JobEndDate__c = job.CompletedDate;
                                    update bsUpdate;

                                    if(jobOrder > 0) {
                                        if(jobOrder == maxOrder)  {
                                            System.Debug('final job projection' );
                                            canPoll = false;
                                            ProjectionsRun = false;                                             
                                            UpdateRunningJobStatus('Projections',false); 
                                            
                                        }
                                        else { 
                                            jobOrder = jobOrder + 1;
                                            System.Debug('start next job order '+jobOrder);
                                            bsUpdateNext = [Select JobName__c,JobStatus__c,JobStartDate__c from BatchStatus__c where JobOrder__c = :jobOrder AND jobType__c = 'Projections'];
                                            bsUpdateNext.JobStatus__c = 'In Process';
                                            bsUpdateNext.JobStartDate__c = DateTime.parse(startDate.format());
                                            update bsUpdateNext;
                                            UpdateWrapperJobStatus(bsUpdateNext.JobName__c,jobOrder,bsUpdateNext.JobStatus__c,'Projections');
                                            
                                        }
                                    }
                                     invbi.jobStatus = job.Status;                                 
                                }
                               
                            }
                       
                        }  
                      
                }
                cache.Session.put('ProjectionsbatchData',JSON.serialize(BrandProjectionsJobs));
          }catch(Exception ex) {
             ProjectionsRun = false;            
             UpdateRunningJobStatus('Projections',false); 
           for ( AsyncApexJob aJob : [ Select id ,Status, ApexClass.Name 
                              from AsyncApexJob where Status!='Aborted' 
                               and Status!='Completed' ] ){ 

                  System.AbortJob(aJob.Id);

                 }       
           Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getStackTraceString())); 	 
          if(job !=null && bsUpdate !=null) {
              bsUpdate.JobErrors__c = bsUpdate.JobErrors__c+'\n'+ex.getMessage();
              update bsUpdate;
          }
       }

    }




    private void pollActuals() {
       String jobName = '';
       Integer jobOrder = 0;
       BatchStatus__c bsUpdate,bsUpdateNext ;
       AsyncApexJob job;
       Integer maxOrder = 0;
          try {
              System.Debug('poll Actuals ' +BrandActualsJobs);
            maxOrder = GetMaxOrder('Actuals');
            for(RunProcessBatchController.Batchinfo invbi: BrandActualsJobs ){
                    if(invbi.jobStatus == 'In Process' && invbi.jobType == 'Actuals') {
                        jobName = invbi.jobName;
                        jobOrder = invbi.jobOrder;
                        break;
                    }
                }
                System.Debug('fetch status for '+jobName+' '+jobOrder);
                if(JobName == '') return ;
                job = [SELECT Id,ExtendedStatus, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems,CreatedBy.Email,JobType,ApexClass.Name,CreatedDate,CompletedDate
                                            FROM AsyncApexJob  
                                    where JobType='BatchApex' and ApexClass.Name = :jobName and CreatedDate >= :System.today() ORDER BY CreatedDate DESC LIMIT 1];
                System.Debug('job details '+job);
                if(job != null){
                        for(RunProcessBatchController.Batchinfo invbi: BrandActualsJobs ) {
                            if(invbi.jobName == job.ApexClass.Name) {
                                invbi.jobsProcessed = job.JobItemsProcessed;
                                invbi.totalJobs = job.TotalJobItems;
                                invbi.failures = job.NumberOfErrors; 
                                invbi.submitDate = job.CreatedDate !=null ? job.CreatedDate.format() : null;
                                invbi.completeDate = job.CompletedDate != null ? job.CompletedDate.format():null;                        
                                if(job.status == 'Completed' && invbi.jobStatus == 'In Process') {
                                    System.Debug('update status to completed '+jobOrder);
                                    bsUpdate = [Select JobStatus__c,JobErrors__c from BatchStatus__c where JobName__c=:invbi.jobName];
                                    invbi.failures = job.NumberOfErrors;
                                    invbi.jobsProcessed = job.JobItemsProcessed;
                                    invbi.jobErrors = job.ExtendedStatus;
                                    bsUpdate.JobStatus__c = job.Status;
                                    bsUpdate.JobErrors__c = job.ExtendedStatus;
                                    bsUpdate.JobEndDate__c = job.CompletedDate;
                                    update bsUpdate;

                                    if(jobOrder > 0) {
                                        if(jobOrder == maxOrder)  {
                                            System.Debug('final job actual' );
                                            canPoll = false;
                                            ActualsRun = false;                                             
                                            UpdateRunningJobStatus('Actuals',false); 
                                            
                                        }
                                        else { 
                                            jobOrder = jobOrder + 1;
                                            System.Debug('start next job order '+jobOrder);
                                            bsUpdateNext = [Select JobName__c,JobStatus__c,JobStartDate__c from BatchStatus__c where JobOrder__c = :jobOrder AND jobType__c = 'Actuals'];
                                            bsUpdateNext.JobStatus__c = 'In Process';
                                            bsUpdateNext.JobStartDate__c = DateTime.parse(startDate.format());
                                            update bsUpdateNext;
                                            UpdateWrapperJobStatus(bsUpdateNext.JobName__c,jobOrder,bsUpdateNext.JobStatus__c,'Actuals');
                                            
                                        }
                                    }
                                     invbi.jobStatus = job.Status;                                 
                                }
                               
                            }
                       
                        }  
                      
                }
                cache.Session.put('ActualsbatchData',JSON.serialize(BrandActualsJobs));
          }catch(Exception ex) {
             ActualsRun = false;            
             UpdateRunningJobStatus('Actuals',false); 
           for ( AsyncApexJob aJob : [ Select id ,Status, ApexClass.Name 
                              from AsyncApexJob where Status!='Aborted' 
                               and Status!='Completed' ] ){ 

                  System.AbortJob(aJob.Id);

                 }       
           Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getStackTraceString())); 	 
          if(job !=null && bsUpdate !=null) {
              bsUpdate.JobErrors__c = bsUpdate.JobErrors__c+'\n'+ex.getMessage();
              update bsUpdate;
          }
       }

    }

    private Integer GetMaxOrder(String jobType) {
        Integer maxOrder = 0;
        AggregateResult arInv = [select MAX(joborder__c) MaxJobOrder from Batchstatus__c where JobType__c = :jobType group by JobType__c  ];
        maxOrder = Integer.valueOf(arInv.get('MaxJobOrder'));
        return maxOrder;
    }

    private void pollInventory() {
       String jobName = '';
       Integer jobOrder = 0;
       BatchStatus__c bsUpdate,bsUpdateNext ;
       AsyncApexJob job;
       Integer maxOrder = 0;
          try {
             
             maxOrder = GetMaxOrder('Inventory');
             System.Debug('max order for inventory '+maxOrder);
             System.Debug('Inventory jobs '+InventoryJobs);

            for(RunProcessBatchController.Batchinfo invbi:InventoryJobs ){
                    if(invbi.jobStatus == 'In Process' && invbi.jobType == 'Inventory') {
                        jobName = invbi.jobName;
                        jobOrder = invbi.jobOrder;
                        break;
                    }
                }
                System.Debug('fetch status for '+jobName+' '+jobOrder);
                if(JobName == '') return ;
                job = [SELECT Id,ExtendedStatus, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems,CreatedBy.Email,JobType,ApexClass.Name,CreatedDate,CompletedDate
                                            FROM AsyncApexJob  
                                    where JobType='BatchApex' and ApexClass.Name = :jobName and CreatedDate >= :System.today() ORDER BY CreatedDate DESC LIMIT 1];
                System.Debug('job details '+job);
                if(job != null){
                        for(RunProcessBatchController.Batchinfo invbi:InventoryJobs ) {
                            if(invbi.jobName == job.ApexClass.Name) {
                                invbi.jobsProcessed = job.JobItemsProcessed;
                                invbi.totalJobs = job.TotalJobItems;
                                invbi.failures = job.NumberOfErrors; 
                                invbi.submitDate = job.CreatedDate !=null ? job.CreatedDate.format() : null;
                                invbi.completeDate = job.CompletedDate != null ? job.CompletedDate.format():null;                        
                                if(job.status == 'Completed' && invbi.jobStatus == 'In Process') {
                                    System.Debug('update status to completed '+jobOrder);
                                    bsUpdate = [Select JobStatus__c,JobErrors__c from BatchStatus__c where JobName__c=:invbi.jobName];
                                    invbi.failures = job.NumberOfErrors;
                                    invbi.jobsProcessed = job.JobItemsProcessed;
                                    invbi.jobErrors = job.ExtendedStatus;
                                    bsUpdate.JobStatus__c = job.Status;
                                    bsUpdate.JobErrors__c = job.ExtendedStatus;
                                    bsUpdate.JobEndDate__c = job.CompletedDate;
                                    update bsUpdate;

                                    if(jobOrder > 0) {
                                        if(jobOrder == maxOrder)  {
                                            canPoll = false;
                                            InventoryRun = false;                                             
                                            UpdateRunningJobStatus('Inventory',false); 
                                            
                                        }
                                        else { 
                                            jobOrder = jobOrder + 1;
                                            System.Debug('start next job order '+jobOrder);
                                            bsUpdateNext = [Select JobName__c,JobStatus__c,JobStartDate__c from BatchStatus__c where JobOrder__c = :jobOrder AND jobType__c = 'Inventory'];
                                            bsUpdateNext.JobStatus__c = 'In Process';
                                            bsUpdateNext.JobStartDate__c = DateTime.parse(startDate.format());
                                            update bsUpdateNext;
                                            updateWrapperJobStatus(bsUpdateNext.JobName__c,jobOrder,bsUpdateNext.JobStatus__c,'Inventory');
                                            
                                        }
                                    }
                                     invbi.jobStatus = job.Status;                                 
                                }
                               
                            }
                       
                        }  
                      
                }
                cache.Session.put('InvbatchData',JSON.serialize(InventoryJobs));
          }catch(Exception ex) {
           InventoryRun = false;
           InventoryErrors = true ;
             UpdateRunningJobStatus('Inventory',false); 
           for ( AsyncApexJob aJob : [ Select id ,Status, ApexClass.Name 
                              from AsyncApexJob where Status!='Aborted' 
                               and Status!='Completed' ] ){ 

                  System.AbortJob(aJob.Id);

                 }       
           Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,ex.getStackTraceString())); 	 
          if(job !=null && bsUpdate !=null) {
              bsUpdate.JobErrors__c = bsUpdate.JobErrors__c+'\n'+ex.getMessage();
              update bsUpdate;
          }
       }
    }

    public PageReference PollJobs(){
       System.Debug('in poll jobs '+InventoryRun+' '+ActualsRun+' '+ProjectionsRun);
       if(InventoryRun && canPoll) pollInventory();        
       else if(ActualsRun && canPoll) pollActuals();
       else if(ProjectionsRun && canPoll) pollProjections();
       return null;    
    }

}