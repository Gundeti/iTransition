public class SteamKeyUpdateController {
    
    
    @AuraEnabled public static Map<string,Inventory__c> getInvLst(string month){
    	list<Inventory__c> invLst = [SELECT BrandCode_Plant_BatchNumber__c,Batch_Number__c,Comments__c,Site__c,Status__c,Brand__r.Brand_code__c 
    									FROM Inventory__c WHERE Status__c !='Eliminated'];
    	
    	Map<string,Inventory__c> InvMap= new Map<string,Inventory__c>();
    	for(Inventory__c i : invLst){
    		InvMap.put(i.BrandCode_Plant_BatchNumber__c,i);
    	}
    	/*
    	system.debug('Key set ------'+InvMap.keySet());
    	for(string s: InvMap.keySet()){
    		system.debug('Key brand  ------'+InvMap.get(s).Brand__r.Brand_code__c);
    	}
    	*/
    	return InvMap;
    }
    
    @AuraEnabled public static boolean updateInventory(list<Inventory__c> InvenLst){
		
		if(!InvenLst.isEmpty()){
			update InvenLst;
			system.debug('Updated -----'+InvenLst);	
			//system.debug('Updated -----');
		}
		
    	return true;
    }
    
    
    
}