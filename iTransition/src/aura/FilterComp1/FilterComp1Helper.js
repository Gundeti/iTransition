({
	
	checkBoxData: function(component, event, helper) {
    
    	var arr1 =[]; //category
    	var arr2 =[]; //Brand
    	var arr3 =[]; //lifecycle
    	var strType =[];
    	if(component.get("v.aircareBox"))
    		arr1.push('Aircare');
    	if(component.get("v.dishcareBox"))
    		arr1.push('Dishcare');
    	if(component.get("v.surfacecareBox"))
    		arr1.push('Surfacecare');
    	
    	if(component.get("v.allAirCareBox"))
    		arr2.push('AllAirCare');
    	if(component.get("v.aDWBox"))
    		arr2.push('adw');
    	if(component.get("v.hDWBox"))
    		arr2.push('hdw');
    	if(component.get("v.mrCleanBox"))
    		arr2.push('mrClean');
    	if(component.get("v.swifferBox"))
    		arr2.push('swiffer');
    		
    	if(component.get("v.aPBox"))
    		arr3.push('AP');
    	if(component.get("v.rHBox"))
    		arr3.push('RH');
    		
    	if(component.get("v.customisedBox"))
    		strType.push('Customized');
    	if(component.get("v.nonCustomisedBox"))
    		strType.push('Non-customized');
    	
    	
        var stDate = component.get("v.rangeStart");
        var endDate = component.get("v.rangeEnd");
		var strGBU = component.get("v.strGBU");
        component.set("v.rangeStart", stDate);
        component.set("v.rangeEnd", endDate);
        
        //alert(arr1+"-------"+arr3+"-------"+arr2+"-------"+stDate+"-------"+endDate+"-------"+strType+"-------"+strGBU);
        var setEvent = component.getEvent("FilterAttribute");
        setEvent.setParams({
            "filters": arr1,
            "brand": arr2,
            "lCycle": arr3,
            "rangeStart":stDate,
            "rangeEnd":endDate,
            "strType" : strType,
            "strGBU" : strGBU
        });
        setEvent.fire();
    },
})