({
	doInit : function(component, event, helper) {
		
		var category = component.get("v.category");
        var lCycle = component.get("v.lCycle");
        var brand = component.get("v.brand");
		var Stype = component.get("v.strType");
		var strGBU = component.get("v.strGBU");
		
		var Sdate = component.get("v.rangeStart");
		var Edate = component.get("v.rangeEnd");
		component.set("v.Spinner", true);
		
		var action = component.get("c.ActualsResult"); 
		action.setParams({ "category":category , "lifecycle":lCycle , "brand":brand , "Stype":Stype , "Sdate":Sdate , "Edate":Edate , "userGBU":strGBU });
		             
        action.setCallback(this, function(a) {
        
        	var stateData = a.getState();
        	if(stateData == "SUCCESS"){
        	
        		result = a.getReturnValue();
        		
        		var Actuals =result['Actuals'];
	            if(Actuals.length > 0){
	            	var theads = Actuals[0].Tableheadings;
	            	component.set("v.Table1Len",theads.length + 1);
	            	//alert(component.get("v.Table1Len"));
	            	var theads1 = [];
	            	for(var i = 0; i<theads.length; i++){
		            	var mon1 = theads[i].split('-')[0];
		            	var year1 = theads[i].split('-')[1];
		            	var year2 = year1.substr(2);
		            	theads1.push(mon1.concat('-', year2));
	            	}
	            	component.set("v.headers",theads1);
	            	//component.set("v.headers",Actuals[0].Tableheadings);
	            	component.set("v.TableData",Actuals);	                
	            }
	            
	            var PastProjections =result['PastProjections'];
	            if(PastProjections.length > 0){
	            	component.set("v.ProjTableData",PastProjections);	                
	            } 
	            
	            var FutureProjections =result['FutureProjections'];
	            if(FutureProjections.length > 0){ 
	            	var fheads = FutureProjections[0].Tableheadings;
	            	component.set("v.Table2Len",fheads.length + 1);
	            	
	            	var fheads1 = [];
	            	for(var i = 0; i<fheads.length; i++){
		            	var mon1 = fheads[i].split('-')[0];
		            	var year1 = fheads[i].split('-')[1];
		            	var year2 = year1.substr(2);
		            	fheads1.push(mon1.concat('-', year2));
	            	}
	            	component.set("v.FutureProjheaders",fheads1);
	            	//component.set("v.FutureProjheaders",FutureProjections[0].Tableheadings);
	            	component.set("v.FutureProjTableData",FutureProjections);	                
	            }
	            
        		console.log('result ---->' + JSON.stringify(result));
      //--------------- Chart data---------
        		var labels = component.get("v.headers");
        		//var labels = Actuals[0].Tableheadings;
        		var custs = [];
        		
        		for(var i = 0; i < Actuals.length; i++){
        		   if(Actuals[i].TableRowHeading != Actuals[i].CategoryName){
		        		var opt = Actuals[i].TableRowValues;
		        		var graphHeading = Actuals[i].TableRowHeading
		        		var colors = ['#FF0000','#FFA500','#FFFF00','#00FF00','#008000','#00FFFF'];	        		
		        		custs.push({
			                            name: 'Actuals~'+Actuals[i].CategoryName+'~'+graphHeading,
			                            color: colors[i],
			                            lineColor: colors[i],
			                            data: opt
			                        });
        		   }
        		}
        		
        		for(var i = 0; i < PastProjections.length; i++){   
        			if(PastProjections[i].TableRowHeading != PastProjections[i].CategoryName){
		        		var opt = PastProjections[i].TableRowValues;
		        		var graphHeading = PastProjections[i].TableRowHeading
		        		var colors = ['#0000FF', '#800080', '#FF00FF','#808080','#FFC0CB','#000080'];	        		
		        		custs.push({
			                            name: 'Projections~'+PastProjections[i].CategoryName+'~'+graphHeading,
			                            color: colors[i],
			                            lineColor: colors[i],
			                            data: opt
			                        });
        			}
        		}
        		
        		var Futlabels = component.get("v.FutureProjheaders");
        		//var Futlabels = FutureProjections[0].Tableheadings;
        		var Futcusts = [];
        		
        		for(var i = 0; i < FutureProjections.length; i++){  
        			if(FutureProjections[i].TableRowHeading != FutureProjections[i].CategoryName){
		        		var opt = FutureProjections[i].TableRowValues;
		        		var graphHeading = FutureProjections[i].TableRowHeading
		        		var colors = ['#00FFFF','#008000','#00FF00','#FF0000','#FFA500','#FFFF00'];	        		
		        		Futcusts.push({
			                            name: 'Projections~'+FutureProjections[i].CategoryName+'~'+graphHeading,
			                            color: colors[i],
			                            lineColor: colors[i],
			                            data: opt
			                        });
		        		}
        		}
        		
        		new Highcharts.Chart({
                        chart: {
                            renderTo: component.find("chart").getElement(),
                            type: 'spline',
                            height: 400
                        },
                        title: {
                            text: '' //'Styling data labels by CSS'
                        },

                        xAxis: {
                            title: {
                                text: 'Month'
                            },
                            categories: labels //['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                        },
                        yAxis: {
                            title: {
                                text: 'SKU Count'
                            }
                        },
                        legend: {
                            enabled: true,
                            //floating: true,
                            verticalAlign: 'bottom',
                            align: 'left',
                            //y:40,
                            layout: 'horizontal'
                            ,maxHeight: 200   
                            
                        },
                        plotOptions: {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                                enableMouseTracking: false
                            }
                        },
                        series: custs
                    });
                    
        		new Highcharts.Chart({
                        chart: {
                            renderTo: component.find("futureChart").getElement(),
                            type: 'spline',
                            height: 400
                        },
                        title: {
                            text: '' //'Styling data labels by CSS'
                        },

                        xAxis: {
                            title: {
                                text: 'Month'
                            },
                            categories: Futlabels //['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                        },
                        yAxis: {
                            title: {
                                text: 'SKU Count'
                            }
                        },
                        legend: {
                            enabled: true,
                            //floating: true,
                            verticalAlign: 'bottom',
                            align: 'left',
                            //y:40,
                            layout: 'horizontal'
                            ,maxHeight: 200   
                            
                        },
                        plotOptions: {
                            line: {
                                dataLabels: {
                                    enabled: true
                                },
                                enableMouseTracking: false
                            }
                        },
                        series: Futcusts
                    });
        		component.set("v.Spinner", false);
        	}
        	        	
        });
        // enqueue the action 
        $A.enqueueAction(action); 
	},
	
	showOppmodal: function(component, event, helper) {
		
		helper.toggleClass(component,'modaldialog','slds-fade-in-');
		helper.toggleClass(component,'backdrop','slds-backdrop--');
		
		var headers = component.get("v.headers");
		var category 	= event.currentTarget.dataset.catagory;
		var lifecycle 	= event.currentTarget.dataset.lifecycle;
		var mnth 	= event.currentTarget.dataset.mnth;
		var mon_yr 	= headers[mnth];
		var mon1 = mon_yr.split('-')[0];
    	var year1 = mon_yr.split('-')[1];
    	var mon = mon1.concat('-20',year1);
		//var status 	= event.currentTarget.dataset.status;
		//var group 	= event.currentTarget.dataset.group;		
		
		//alert(category+'==='+lifecycle+'==='+mon+'==='+mnth);
		var lcycle = [];
		
		if(category == lifecycle){
			lcycle.push('ACTIVE & PLANNED','REMNANT & HISTORICAL');
			lifecycle = 'A & P , R & H';
		}
		
		else if(lifecycle == 'A & P')
		{
			lcycle.push('ACTIVE & PLANNED');
		}

		else if(lifecycle == 'R & H')
		{
			lcycle.push('REMNANT & HISTORICAL');
		}
		//alert(category+'==='+lcycle+'==='+mon+'==='+mnth);
	
		document.getElementById("CATEGORY").innerHTML = category;
		document.getElementById("MONTH").innerHTML = mon;
		document.getElementById("lifecycle").innerHTML = lifecycle;
		
		helper.getSKUDynamicData(component,category,lcycle,mon);
		
	},
	
	hideModal : function(component, event, helper) {
		 //Toggle CSS styles for hiding Modal
		helper.toggleClassInverse(component,'backdrop','slds-backdrop--');
		helper.toggleClassInverse(component,'modaldialog','slds-fade-in-');
		
	},
	
	exportFile: function(cmp,evt,helper){
		helper.exportToExcelAll(cmp);
	},
	
	PageDirection: function(component, event, helper) {
    	var page = component.get("v.page");
        //var recordToDisply = '10';
       // var direction = event.getSource().get("v.value");
       	var direction = event.currentTarget.dataset.dir;
        page=page*1;
        page = direction === "previous" ? (page - 1) : (page + 1);        
    	helper.pageskus(component, page);
    },

    pageNum : function(component, event, helper) {
        
        var recordToDisply = '10';	//component.find("recordSize").get("v.value"); 
        var page = event.currentTarget.dataset.index;
        helper.pageskus(component, page); 
        
    },
	

})