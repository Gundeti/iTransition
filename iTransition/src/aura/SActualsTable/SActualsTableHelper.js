({
	pageskus : function(component, page) {
        var totalskus=component.get("v.lstSKUs");
        var recordToDisply = component.get("v.pageSize");
        var total = totalskus.length;
        var pages = Math.ceil(total / recordToDisply) ;
        var offset= (page-1)*recordToDisply;
        var pagesku = [];
        
        for(var i=offset;i<(total<(page*recordToDisply) ? total:page*recordToDisply);i++)
        {
            pagesku.push(totalskus[i]); 
        }
        component.set("v.lstSKUsData",pagesku);
        component.set("v.page", page);
        component.set("v.total", total);
        component.set("v.pages", pages);
        
        var pnums = []; 
        var noOfPages=pages<10?pages:10;
        //alert(noOfPages);
        for(var i=1;i<=noOfPages;i++){
            pnums.push(i);
        }
        component.set("v.pagenumbers",pnums);
    },
	toggleClass: function(component,componentId,className) {
		var modal = component.find(componentId);
		$A.util.removeClass(modal,className+'hide');
		$A.util.addClass(modal,className+'open');
	},

	toggleClassInverse: function(component,componentId,className) {
		var modal = component.find(componentId);
		$A.util.addClass(modal,className+'hide');
		$A.util.removeClass(modal,className+'open');
	},
	
	getSKUDynamicData : function(component,category,lcycle,mon) {
		component.set("v.lstSKUs",null);
		component.set("v.lstSKUsData",null);
		component.set("v.total",0);
		component.set("v.pages",1);
		component.set("v.page",1);
		component.set("v.pagenumbers",null);
		//document.getElementById("Accspinner1").style.display = "block";
		//alert(document.getElementById("CATEGORY").innerHTML+' ===#'+group+' ===#'+document.getElementById("MONTH").innerHTML+' ===#'+status+' ===#'+brandval+' ===#'+document.getElementById("lifecycle").innerHTML);
        var brand = component.get("v.brand");
        var Stype = component.get("v.strType");
        //var userGBU = component.get("v.strGBU");
        var strGBU = component.get("v.strGBU");
        
        component.set("v.Spinner", true);
        
        var action = component.get("c.fetchSKUdetails");
        action.setParams({
                            "category":category,
                            "lifecycle": lcycle,
							"brand":brand,
							"Stype":Stype,
							"userGBU": strGBU,
							"mon_year":mon
                        });
        //Setting the Callback
            action.setCallback(this,function(a){
                //get the response state
                var state = a.getState();
                
                //check if result is successfull
                if(state == "SUCCESS"){
                    var result = a.getReturnValue();
                    if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                    	//component.set("v.lstDynamicData",result);
                    	component.set("v.lstSKUs",result);
                    	
					//document.getElementById("Accspinner1").style.display = "none";
                } else if(state == "ERROR"){
                	//document.getElementById("Accspinner1").style.display = "none";
                    alert('Error in calling server side action');
                }
                component.set("v.Spinner", false);
                this.pageskus(component,1);
            });
            
            //adds the server-side action to the queue        
            $A.enqueueAction(action);
	},
	
	
	exportToExcelAll: function(cmp){
		var helper=this;
		var actualRequired = false;
		var pastRequired = false;
		var futureRequired = false;
		
		var action = cmp.get("c.exportToExcelSettings");
		
	    action.setCallback(this, function(response){
	    	//alert(response.getState());
	    	var profUrl = $A.get('$Resource.test');
	    	var progtmp = $A.get('{!$Resource.test}');
	    	//alert(profUrl);
	    	//alert(progtmp);
	    	
	    	if(cmp.isValid() && response !== null && response.getState() == 'SUCCESS'){
	        	 var result = response.getReturnValue();
	        	 
	            //alert(JSON.stringify(result));
	            if(result!=null && result.length>0)
	            for(var i=0; i< result.length; i++)
	            {
	            	if(result[i].Name == 'Actual' && result[i].Required__c == true)
		            {
		            	actualRequired = true;
		            }
		            else if(result[i].Name == 'PastProjections' && result[i].Required__c == true)
		            {
		            	pastRequired = true;
		            }
		            else if(result[i].Name == 'FutureProjections' && result[i].Required__c == true)
		            {
		            	futureRequired = true;
		            }
	            }
	            
	            
	            //actualRequired = true;
	            //pastRequired = true;
	            //futureRequired = true;
	            
	            var wb = XLSX.utils.book_new();
				
				
				var datArr=[];
				var header=[];
				var rows=[];
				
				var headers1 = cmp.get("v.headers");
				var ActTableData = cmp.get("v.TableData");
				var ProjTableData = cmp.get("v.ProjTableData");
				
				var futProheaders1 = cmp.get("v.FutureProjheaders");
				var FutureProjTableData = cmp.get("v.FutureProjTableData");
				
				if(actualRequired == true)
				{
					header.push('');
					
					for(var k=0; k<headers1.length; k++)
					{
						header.push(headers1[k]);
					}
					for(var i=0;i<ActTableData.length;i++){
						var aclst = ActTableData[i].TableRowValues;
						var tempRow1 = []; //new Array(header.length);
						
						var RowHeading = ActTableData[i].TableRowHeading;
						tempRow1.push(RowHeading);
						
						for(var k=0;k<aclst.length;k++){
							len++;
							tempRow1.push(aclst[k]);
						}
						
						rows.push(tempRow1);
					}
						
					//row2.push('Actuals');
					
					datArr.push(header);
					//datArr.push(rows);
					
					if(rows!=null && rows.length>0)
					{
						for(var z=0; z< rows.length; z++)
						{
							datArr.push(rows[z]);
						}
					}
					
			        datArr.push([]);
			        datArr.push([]);
			        datArr.push([]);
		        }
		        
		        if(pastRequired == true)
				{
					var header=[];
					var rows=[];
					
					header.push('');
					
					for(var k=0; k<headers1.length; k++)
					{
						header.push(headers1[k]);
					}
					
					for(var i=0;i<ProjTableData.length;i++){
						var prlst = ProjTableData[i].TableRowValues;
						
						var tempRow1 = [];  //new Array(header.length);
						
						var RowHeading = ProjTableData[i].TableRowHeading;
						tempRow1.push(RowHeading);
						
						for(var k=0;k<prlst.length;k++){
							tempRow1.push(prlst[k]);
						}
						
						rows.push(tempRow1);
					}
						
					//row2.push('Actuals');
					
					datArr.push(header);
					//datArr.push(rows);
					
					if(rows!=null && rows.length>0)
					{
						for(var z=0; z< rows.length; z++)
						{
							datArr.push(rows[z]);
						}
					}
					
			        datArr.push([]);
			        datArr.push([]);
			        datArr.push([]);
		        }
		        
		        //var futProheaders1 = cmp.get("v.FutureProjheaders");
				//var FutureProjTableData = cmp.get("v.FutureProjTableData");
		        
		        if(futureRequired == true)
				{
					var header=[];
					var rows=[];
					
					header.push('');
					
					for(var k=0; k<futProheaders1.length; k++)
					{
						header.push(futProheaders1[k]);
					}
					
					for(var i=0;i<FutureProjTableData.length;i++){
						var ftplst = FutureProjTableData[i].TableRowValues;
						
						var tempRow1 = []; //new Array(header.length);
						
						var RowHeading = FutureProjTableData[i].TableRowHeading;
						tempRow1.push(RowHeading);
						
						for(var k=0;k<ftplst.length;k++){
							tempRow1.push(ftplst[k]);
						}
						
						rows.push(tempRow1);
					}
						
					//row2.push('Actuals');
					
					datArr.push(header);
					//datArr.push(rows);
					
					if(rows!=null && rows.length>0)
					{
						for(var z=0; z< rows.length; z++)
						{
							datArr.push(rows[z]);
						}
					}
					
			        datArr.push([]);
			        datArr.push([]);
			        datArr.push([]);
		        }
		        
		        if(actualRequired == true || pastRequired == true || futureRequired == true)
		        {
					var ws = XLSX.utils.aoa_to_sheet(datArr);
			        //var range = {s:{c:2,r:2},e:{c:8,r:2}};
			        //ws['!ref']=XLSX.utils.encode_range(range);
					
			        XLSX.utils.book_append_sheet(wb, ws, 'Projections');
			        
					var wbout = XLSX.write(wb,{bookType:'xlsx',bookSST:true, type:'binary'});
			        
					saveAs(new Blob([helper.s2ab(wbout)],{type:"application/octet-steam"}),"Projections.xlsx");
				}else
				{
					alert($A.get("$Label.c.ExportAlert"));
				}
	        }
	        
	    });
	
	    $A.enqueueAction(action);
		
	},
	exportToExcel: function(cmp){
		

		var ws = XLSX.utils.aoa_to_sheet(datArr);

        /*
		var range = {s:{c:1,r:1},e:{c:actualsAllJSON.length,r:2}};
		ws['!ref']=XLSX.utils.encode_range(range);
		
		wb.SheetNames.push('Actuals');
		wb.Sheets['Actuals'] = ws;
        */

        XLSX.utils.book_append_sheet(wb, ws, 'Actuals');
		var wbout = XLSX.write(wb,{bookType:'xlsx',bookSST:true, type:'binary'});
        
		saveAs(new Blob([helper.s2ab(wbout)],{type:"application/octet-steam"}),"test.xlsx");
	},

	s2ab: function(s) {
		if(typeof ArrayBuffer !== 'undefined') {
			var buf = new ArrayBuffer(s.length);
			var view = new Uint8Array(buf);
			for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
			return buf;
		} else {
			var buf = new Array(s.length);
			for (var i=0; i!=s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
			return buf;
		}
	}
	
	
})