({

    save : function(component) {
        
        var action = component.get("c.updateInventory");
        
        action.setParams({ "InvenLst": component.get("v.InvLst") });
        
        action.setCallback(this, function(response){
            
        	var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
                result = response.getReturnValue();
                
                this.showToast(component, "Success!  ", "Status updated Successfully ", "success");
                /*component.set("v.InvLstOld",null);
                component.set("v.InvLst",null);
                component.find("file").getElement().value = "";*/
            }
        })        
		$A.enqueueAction(action); 
    },
    
    contains : function(lst, obj) {
	    var i = lst.length;
	    while (i--) {
	        if (lst[i] == obj) {
	            return true;
	        }
	    }
	    return false;
    },
    
    showToast: function(component, title, message, type){
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            toastEvent.setParams({ 
                "title": title,
                "message": message,
                "type" : type,
                "mode" : "dismissible",
                "duration" : "30000"
            });    
        	toastEvent.fire();
        } else {
            var toastTheme = component.find("toastTheme");
            
            $A.util.removeClass(component.find("customToast"), "slds-hide");
            $A.util.removeClass(toastTheme, "slds-theme--error");
        	$A.util.removeClass(toastTheme, "slds-theme--success");
            
            if(type == 'error'){
                $A.util.addClass(toastTheme, "slds-theme--error");
            } else if(type == 'success'){
                $A.util.addClass(toastTheme, "slds-theme--success");
            }
            component.find("toastText").set("v.value", message);
            component.find("toastTtitle").set("v.value", title+' ');
        }
        //alert("showing helper");
    },
    
    closeToast: function(component){
        var toastTheme = component.find("toastTheme");
        $A.util.addClass(component.find("customToast"), "slds-hide");
        $A.util.removeClass(toastTheme, "slds-theme--error");
        $A.util.removeClass(toastTheme, "slds-theme--success");
    },
    
    
})