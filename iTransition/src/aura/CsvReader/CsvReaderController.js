({

	doInit:function(component,event,helper) {
		//alert("Commimg");
        var action = component.get("c.getInvLst");
        
        action.setCallback(this, function(response){
            
        	var state = response.getState();
            if (component.isValid() && state === "SUCCESS") {
            	var result = new Map();
                result = response.getReturnValue();
                component.set("v.InvLstMap", response.getReturnValue());
                var keyvals = [];
                for ( key in result ) {
                    keyvals.push(key);
                }
                component.set("v.keyValAry", keyvals);
                //alert(component.get("v.InvLstMap")[keyvals[0]].Brand__r.Brand_code__c); 

            }
        })        
		$A.enqueueAction(action); 
		
	},

	fileSelected : function(component, event, helper) {
		
		component.set("v.isPreview",false);
		component.set("v.InvLstOld",null);
		
	    var file = component.find("file").getElement().files[0];
	    console.log('file'+file);
	    //alert("file sizeeee----"+file.size);
	    
        
        if (file) {
        var reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = function (evt) {
        	//alert("loading");
            var csv1 = evt.target.result;
            //alert("result"+result.split("\n")[0].split(",").length); 
            var lines=csv1.split("\n");

			var result = [];
	
			var headers=lines[0].split(",");
			component.set("v.headers",headers);
			
			
			
			for(var i=1;i<lines.length;i++){
	
				var obj = {};
				var currentline=lines[i].split(",");
		
				for(var j=0;j<headers.length;j++){
				obj[headers[j]] = currentline[j];
				}
		
				result.push(obj);
            }
            
			component.set("v.recs",result);
            
            result = [];
            InvLstOld = [];
            invLstMp = new Map;
            
            for(var i=1;i<lines.length;i++){
	
				var obj = {};
				var currentline=lines[i].split(",");
				
				if(helper.contains(component.get("v.keyValAry"),currentline[0])){
					var inv = component.get("v.InvLstMap")[currentline[0]];
					//inv.Status__c = 'Submitted for Steaming';
					//alert(component.get("v.InvLstMap")[currentline[0]].Brand__c.Brand_code__c);
					
					for(var j=0;j<headers.length;j++){
						if(headers[j].toLowerCase().includes('steam'.toLowerCase()))
							invLstMp[currentline[0]] =  currentline[j];
						
						obj[headers[j]] = currentline[j];
					}
					
					InvLstOld.push(inv);
					result.push(obj);	
				}
				
            }
            /*var keys = [], i = 0;
            for (keys[i++] in invLstMp) {}
            alert(keys.length);*/
            component.set("v.steamMap",invLstMp);
            component.set("v.InvLstOld",InvLstOld);
            component.set("v.matchRecs",result);
            
            
            if(InvLstOld.length == 0){
            	alert(InvLstOld + InvLstOld.length);
            	helper.showToast(component, "Error!  ", "No Mactching records found", "error");
            }
			
			/*
			
			filteredheads = [];

			for(var i = 0 ; i<headers.length ; i++){
				if(helper.contains(filteredheads,headers[i])){
					filteredheads.push(headers[i]);
				}
				
				
			}
			
			var filtHeaders;
			if(headers.length>0) 
				filtHeaders = headers.map(val=>{
						return /key/i.test(val)?"steam key":
							/brand/i.test(val)?"Brand Code":
							/Plant/i.test(val)?"Site":
							/batch/i.test(val)?"Batch Number":"";
					});
					
			*/
            
	        }
	        reader.onerror = function (evt) {
	            //console.log("error reading file");
	            //alert("error reading file");
	            helper.showToast(component, "Error!  ", "Error reading file", "error");
	        }
	    }
	    
	},
	
	updateInv : function(component, event, helper) {
		var stMap = component.get("v.steamMap");
		var newInv = [];
		for(var ky in stMap){
			var inv = component.get("v.InvLstMap")[ky];
			var stmkey = component.get("v.steamMap")[ky];
			inv.Comments__c = stmkey;
			inv.Status__c = 'Submitted for Steaming';
			newInv.push(inv);
			//alert(stmkey);
		}
		alert(newInv.length); 
		component.set("v.InvLst",newInv);
		component.set("v.isPreview",true);
		//helper.save(component);
		
	},
	
	save : function(component, event, helper) {
		helper.save(component);
	
	},
	msg : function(component, event, helper) {
        //helper.save(component);
        
        var fileInput = component.find("file").getElement();
    	var file = fileInput.files[0];
    	var ext = file.name.substring(file.name.length-3,file.name.length);
    	var fileName = component.find("file-name");
    	alert(fileName + file.name);
    	component.find("file-name").set("v.value", " "+file.name);
    	if(ext.toLowerCase() != 'csv'){
    		component.find("file").getElement().value = "";
    		helper.showToast(component, "Error!  ", "Kindly select CSV file Only", "error");
    	}
    },
    
    closeCustomToast: function(component, event, helper){
        helper.closeToast(component);
    },
    
    deleteRec : function(component, event, helper) {
        
        var ky1= event.currentTarget.dataset.key;
        //alert(ky1);
        var stMap = component.get("v.steamMap");
        delete stMap[ky1];
        //alert("Deleted "+ky1);
    },
})