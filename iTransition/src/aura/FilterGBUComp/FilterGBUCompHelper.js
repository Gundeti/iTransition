({
	hideAllTabs: function(component) {
    
        component.set("v.firstComp", true);
        component.set("v.secondComp", true);	//It should be true, because brand tab is dependent on Category
        component.set("v.thirdComp", false);
        component.set("v.forthComp", false);
        component.set("v.fifthComp", false);

    },
    
    markDefaultCatBnd: function(component) {
    
    	var defCat = component.get("v.defaultCategory");
        //alert(defCat);
        var getAllcats = component.find("category");
        var seleCats = [];
        
        component.set("v.term", []);
        
        for(var i = 0; i<getAllcats.length; i++){
        	if(getAllcats[i].get("v.text") == defCat){
        		component.find("category")[i].set("v.value", true);
        	}
        	else{
        		component.find("category")[i].set("v.value", false);
        	}
        }
        seleCats.push(defCat);
        component.set("v.term", seleCats);
        this.markBrandsOfSelectedCats(component);
    },
    
    markBrandsOfSelectedCats: function(component) {
    	var seleCats = component.get("v.term");
    	
    	component.set("v.brand", []);
    	
        var seleBrands = [];
        for(var i = 0; i< seleCats.length; i++){
        	var bnds = component.get("v.catbrandmap")[seleCats[i]];
        	
        	for(var j = 0; j<bnds.length; j++){
        		seleBrands.push(bnds[j]);
        	}
        	
        }
        var getAllbnds = component.find("brand");
        
        for(var i = 0; i<getAllbnds.length; i++){
        	component.find("brand")[i].set("v.value", false);
        	
        	for(var j = 0; j<seleBrands.length; j++){
        		if(getAllbnds[i].get("v.text") == seleBrands[j]){
	        		component.find("brand")[i].set("v.value", true);
	        	}
        	}
        	
        }
        component.set("v.brand", seleBrands);
    },
    
    markCatsOfSelectedBrands: function(component) {
    	var seleBands = component.get("v.brand");
    	var pagCats = component.get("v.pageCategories");
    	var seleCats = [];
    	for(var i = 0; i<pagCats.length; i++){
    		var CatBnds = component.get("v.catbrandmap")[pagCats[i]];
    		for(var j = 0; j<seleBands.length; j++){
    			var IsincludedInCat = this.contains(CatBnds,seleBands[j]);
    			if(IsincludedInCat){
    				seleCats.push(pagCats[i]);
    				break;
    			}
    		}
    	}
    	//alert(seleCats);
    	component.set("v.term", seleCats);
    	var getAllcats = component.find("category");
        
        for(var i = 0; i<getAllcats.length; i++){
        	component.find("category")[i].set("v.value", false);
        	for(var j = 0; j<seleCats.length; j++){
        		
	        	if(getAllcats[i].get("v.text") == seleCats[j]){
	        		component.find("category")[i].set("v.value", true);
	        		//break; 
	        	}
        	}
        }
    },
    
    contains : function(lst, obj) {
	    var i = lst.length;
	    while (i--) {
	        if (lst[i] == obj) {
	            return true;
	        }
	    }
	    return false;
    },
    
    checkBoxData: function(component, event, helper) {
    
    	var arr1 =[]; //category
    	var arr2 =[]; //Brand
    	var arr3 =[]; //lifecycle
    	var strType =[];
    	
    	arr1 = component.get("v.term");
    	arr2 = component.get("v.brand");
    		
    	if(component.get("v.aPBox"))
    		arr3.push('AP');
    	if(component.get("v.rHBox"))
    		arr3.push('RH');
    		
    	if(component.get("v.customisedBox"))
    		strType.push('Customized');
    	if(component.get("v.nonCustomisedBox"))
    		strType.push('Non-customized');
    	
        var stDate = component.get("v.rangeStart");
        var endDate = component.get("v.rangeEnd");
		var strGBU = component.get("v.strGBU");
        component.set("v.rangeStart", stDate);
        component.set("v.rangeEnd", endDate);
        
        //alert(arr1+"-------"+arr3+"-------"+arr2+"-------"+stDate+"-------"+endDate+"-------"+strType+"-------"+strGBU);
        var setEvent = component.getEvent("FilterAttribute");
        setEvent.setParams({
            "filters": arr1,
            "brand": arr2,
            "lCycle": arr3,
            "rangeStart":stDate,
            "rangeEnd":endDate,
            "strType" : strType,
            "strGBU" : strGBU
        });
        setEvent.fire();
    },
    
})