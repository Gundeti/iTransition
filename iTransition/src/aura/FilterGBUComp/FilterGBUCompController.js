({
	init: function(component, event, helper) {
	
        //alert(component.get("v.gbucatmaptmp")['Home Care']);
        
        component.set("v.firstComp",true);
        component.set("v.secondComp",true);
        component.set("v.thirdComp",false);
        component.set("v.forthComp",false);
        component.set("v.fifthComp",false);        
        
        var defaultCategory = '';
        var defaultGBU = component.get("v.strGBU");
        
        var action = component.get("c.userSettings");

	    action.setCallback(this, function(response){
	        if(component.isValid() && response !== null && response.getState() == 'SUCCESS'){
	        	 var result = response.getReturnValue();
	        	 
	        	 var lstUser;
	        	 var startDate;
	        	 var endDate;
	        	 if(result !=null ){
	        		 lstUser = result.lstUser;
	        		 startDate = result.startDate;
	        		 endDate = result.endDate;
	        	 }
	        	 if(endDate != null && startDate != null){
	        		 if($A.util.isEmpty(component.get('v.rangeStart')) || $A.util.isUndefined(component.get('v.rangeStart'))){
	        			 component.set('v.rangeStart',startDate);
	        		 }
	        		 if($A.util.isEmpty(component.get('v.rangeEnd')) || $A.util.isUndefined(component.get('v.rangeEnd'))){
	        			 component.set('v.rangeEnd',endDate);
	        		 }
	        	 }
	        	 //alert(JSON.stringify(result));
	        	 if(lstUser!=null && lstUser.length>0)
        		 for(var i=0; i< lstUser.length; i++)
        		 {
        			 if (!$A.util.isEmpty(lstUser[i].GBU__c) && !$A.util.isUndefined(lstUser[i].GBU__c))
        			 {
        				 defaultGBU = lstUser[i].GBU__c;
        				 
        				 if (!$A.util.isEmpty(lstUser[i].Category__c) && !$A.util.isUndefined(lstUser[i].Category__c))
	        			 {
	        				 defaultCategory = lstUser[i].Category__c;
	        			 }
	        			 
        			 }else
        			 {
        				 defaultGBU = 'Home Care';
        				 defaultCategory = 'Aircare';
        			 }
        			 
        		 }
        		 if ($A.util.isEmpty(defaultCategory) || $A.util.isUndefined(defaultCategory)){
        			 defaultCategory = component.get("v.gbucatmap")[defaultGBU][0];
        		 }
        		 	component.set("v.strGBU",defaultGBU);
        		 	component.set("v.defaultCategory",defaultCategory);
        		 	
					component.set("v.pageCategories",component.get("v.gbucatmap")[component.get("v.strGBU")]);
					component.set("v.pageBrands",component.get("v.gbubrandmap")[component.get("v.strGBU")]);
					
					helper.markDefaultCatBnd(component);
        		 
				
				var arr1 = component.get("v.term");
		        var arr2 = component.get("v.lCycle");
		        var arr3 = component.get("v.brand");
		        var stDate = component.get("v.rangeStart");
		        var endDate = component.get("v.rangeEnd");
		        var strTtype = component.get("v.strType");
		        //alert(arr1+'='+arr2+'='+arr3+'='+stDate+'='+endDate+'='+strTtype);
		        
		        var setEvent = component.getEvent("FilterAttribute");
                
		        setEvent.setParams({
		            "filters": arr1,
		            "lCycle": arr2,
		            "brand": arr3,
		            "rangeStart":stDate,
		            "rangeEnd":endDate,
		            "strType" : strTtype,
		            "strGBU" : defaultGBU
		        });
		        setEvent.fire();
		        
		        
		        
		        
		        
	        }
	    });
	    $A.enqueueAction(action);       

    },
    tabAction: function(component, event, helper) {
    	
    	var tabId= event.currentTarget.dataset.index;
    	helper.hideAllTabs(component);
        for(var i=1;i<=6;i++) {
			var tmptab = component.find('t'+i.toString());
			var tmptabDetail = component.find('tab'+i.toString());
			//alert(tmptab);
			$A.util.removeClass(tmptab, 'slds-active');
			$A.util.removeClass(tmptabDetail, 'slds-show');
			$A.util.addClass(tmptabDetail, 'slds-hide');
		}
		
		
		//alert(tabId.slice(1));
		var ActiveTab = component.find(tabId);
		var ActiveTabDetail = component.find('tab'+tabId.slice(1));
		
		$A.util.addClass(ActiveTab, 'slds-active');
		$A.util.removeClass(tmptabDetail, 'slds-hide');
		$A.util.addClass(ActiveTabDetail, 'slds-show');
        if(tabId.slice(1) == "1"){
        	component.set("v.firstComp", true);
        }
        if(tabId.slice(1) == "2"){
        	component.set("v.secondComp", true);
        }
        if(tabId.slice(1) == "3"){
        	component.set("v.thirdComp", true);
        }
        if(tabId.slice(1) == "4"){
        	component.set("v.forthComp", true);
        }
        if(tabId.slice(1) == "5"){
        component.set("v.fifthComp", true);
        }
        
        //alert(component.get("v.firstComp")+"--"+component.get("v.secondComp")+"--"+component.get("v.thirdComp")+"--"+component.get("v.forthComp")+"--"+component.get("v.fifthComp"));

    },
    catCheckboxChange: function(component, event, helper) {
    
        var getAllcats = component.find("category");
        var seleCats = [];
        
        component.set("v.term", []);
        for(var i = 0; i<getAllcats.length; i++){
        	if(getAllcats[i].get("v.value") == true){
        		seleCats.push(getAllcats[i].get("v.text"));
        	}
        }
        
        component.set("v.term", seleCats);
        helper.markBrandsOfSelectedCats(component);
        
        if(seleCats.length == 0){
        	//seleCats.push(component.get("v.pageCategories")[0]);
        	helper.markDefaultCatBnd(component);
        }
        //alert(component.get("v.term"));
        
        var setEvent = component.getEvent("FilterAttribute");
        setEvent.setParams({
            "filters": component.get("v.term"),
            "lCycle": component.get("v.lCycle"),
            "brand": component.get("v.brand"),
            "rangeStart":component.get("v.rangeStart"),
            "rangeEnd":component.get("v.rangeEnd"),
            "strType" : component.get("v.strType"),
            "strGBU" : component.get("v.strGBU")
        });
        setEvent.fire();
    },
    
    brandCheckboxChange: function(component, event, helper) {
    
        var getAllbnds = component.find("brand");
        var seleBands = [];
        
        component.set("v.brand", []);
        for(var i = 0; i<getAllbnds.length; i++){
        	if(getAllbnds[i].get("v.value") == true){
        		seleBands.push(getAllbnds[i].get("v.text"));
        	}
        }
        component.set("v.brand", seleBands);
        
        if(seleBands.length == 0){
        	//seleBands.push(component.get("v.pageBrands")[0]);
        	helper.markDefaultCatBnd(component);
        }
        helper.markCatsOfSelectedBrands(component);
        //alert(component.get("v.brand")); 
        
        
        var setEvent = component.getEvent("FilterAttribute");
        setEvent.setParams({
            "filters": component.get("v.term"),
            "lCycle": component.get("v.lCycle"),
            "brand": component.get("v.brand"),
            "rangeStart":component.get("v.rangeStart"),
            "rangeEnd":component.get("v.rangeEnd"),
            "strType" : component.get("v.strType"),
            "strGBU" : component.get("v.strGBU")
        });
        setEvent.fire();
    },
    
    lCChange: function(component, event, helper) {
        var cate = event.target.id;
        //alert(cate+'===='+event.target.checked);
        //var appEvent = $A.get("e.c:FilterEvent");
        var arr2 = component.get("v.lCycle");
        var arr1 = component.get("v.term");
        var arr3 = component.get("v.brand");
        var stDate = component.get("v.rangeStart");
        var endDate = component.get("v.rangeEnd");
		var strGBU = component.get("v.strGBU");
        if (cate == 'AP') {
            component.set("v.aPBox", event.target.checked);
        } else if (cate == 'RH') {
            component.set("v.rHBox", event.target.checked);
        }
        if (event.target.checked) {
            arr2.push(cate);
        } else {
            for (var i = 0; i < arr2.length; i++) {
                if (arr2[i] == cate) {
                    arr2.splice(i, 1);
                }
            }
        }
        if(arr2.length == 0){
        	arr2.push('AP');
        	component.set("v.aPBox", true);
        }
        component.set("v.lCycle", arr2);
        var strTtype = component.get("v.strType");
        
        var setEvent = component.getEvent("FilterAttribute");
        setEvent.setParams({
            "filters": arr1,
            "lCycle": arr2,
            "brand": arr3,
            "rangeStart":stDate,
            "rangeEnd":endDate,
            "strType" : strTtype,
            "strGBU" : strGBU
        });
        setEvent.fire();
    },
   
    nonCustomizedChange: function(component, event, helper) {
        var cate = event.target.id;

        //alert(cate+'===='+event.target.checked);
        //var appEvent = $A.get("e.c:FilterEvent");
        var arr2 = component.get("v.lCycle");
        var arr1 = component.get("v.term");
        var arr3 = component.get("v.brand");
        var stDate = component.get("v.rangeStart");
        var endDate = component.get("v.rangeEnd");
        var strTtype = component.get("v.strType");
		var strGBU = component.get("v.strGBU");
        
        if (cate == 'Customized') {
            component.set("v.customisedBox", event.target.checked);
        } else if (cate == 'Non-customized') {
            component.set("v.nonCustomisedBox", event.target.checked);
        }
        
        if (event.target.checked) {
            strTtype.push(cate);
        } else {
            for (var i = 0; i < strTtype.length; i++) {
                if (strTtype[i] == cate) {
                    strTtype.splice(i, 1);
                }
            }
        }
        if(strTtype.length == 0){
        	strTtype.push('Non-customized');
        	//strTtype = ['Non-customized', 'Customized'];
            //component.set("v.customisedBox", 'true');
            component.set("v.nonCustomisedBox", 'true');
        }
        component.set("v.strType", strTtype);
        
        var setEvent = component.getEvent("FilterAttribute");
        setEvent.setParams({
            "filters": arr1,
            "lCycle": arr2,
            "brand": arr3,
            "rangeStart":stDate,
            "rangeEnd":endDate,
            "strType" : strTtype,
            "strGBU" : strGBU
        });
        setEvent.fire();
    },
    setOutput: function(component, event, helper) {
        
        //var appEvent = $A.get("e.c:FilterEvent");
        var arr2 = component.get("v.lCycle");
        var arr1 = component.get("v.term");
        var arr3 = component.get("v.brand");
        var stDate = component.get("v.rangeStart");
        var endDate = component.get("v.rangeEnd");
        var strTtype = component.get("v.strType");
		var strGBU = component.get("v.strGBU");
        //alert(stDate+'==='+endDate);
        component.set("v.rangeStart", stDate);
        component.set("v.rangeEnd", endDate);
        
        var setEvent = component.getEvent("FilterAttribute");
        setEvent.setParams({
            "filters": arr1,
            "lCycle": arr2,
            "brand": arr3,
            "rangeStart":stDate,
            "rangeEnd":endDate,
            "strType" : strTtype,
            "strGBU" : strGBU
        });
        setEvent.fire();
    },
    
    refrsh: function(component, event, helper) {
    	helper.checkBoxData(component, event, helper);
    },
    
})