({
	handleFilterEvent : function(component, event, helper) {
    	
		var category = event.getParam("filters");
		var lCycle = event.getParam("lCycle");
		var Stype = event.getParam("strType");
		var brand = event.getParam("brand");
		var rangeStart = event.getParam("rangeStart");
		var rangeEnd = event.getParam("rangeEnd");
		var strGBU = event.getParam("strGBU");
        
        var category1=[];var lCycle1=[]; var brand1=[]; var Stype1=[];
        
        category1 = category;
        brand1 = brand ;

        /*       
        for(var i=0;i<category.length;i++)
        {category1[i]=component.get("v.filterLabel")[category[i]];}
        
		for(var i=0;i<brand.length;i++)
        {brand1[i]=component.get("v.filterLabel")[brand[i]];}    
        
        */
        
        for(var i=0;i<lCycle.length;i++)
        {lCycle1[i]=component.get("v.filterLabel")[lCycle[i]];}
        
        for(var i=0;i<Stype.length;i++)
        {Stype1[i]=component.get("v.filterLabel")[Stype[i]];}
        
        /*component.set("v.category", category1);
        component.set("v.lCycle", lCycle1);
        component.set("v.brand", brand1);
        component.set("v.strType", Stype1);
        component.set("v.strGBU", strGBU);*/
        
        //alert('MyGlidePath='+filt+' = '+lCyc+' = '+strbrand+' = '+rangeStart+' = '+rangeEnd +' = '+strType);
        var container = component.find("glideContainer");
		if (!$A.util.isEmpty(container) && !$A.util.isUndefined(container))
        {
	    	$A.createComponent("c:SActualsTable",
				{
					"category": category1,
					"lCycle": lCycle1,
					"brand" : brand1,
					"strType" : Stype1,
					"strGBU" : strGBU,
					"rangeStart" : rangeStart,
					"rangeEnd" :rangeEnd,
				},
			function(comp){
				container.set("v.body",[comp]); 
				//$A.get("e.c:renderdynamicvals").fire();
				//helper.hideSpinner(cmp);
			});
		}
        
    },
        
})